<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get("/", "Dashboard\HomeController@index");
Route::get("our-tim", "Dashboard\HomeController@our_tim");
Route::get("our-work/{id}", "Dashboard\HomeController@our_work");
Route::post("kontak-kita", "Dashboard\HomeController@contact_us");
Route::get("cek-berkas", "Dashboard\HomeController@cek_berkas");

Route::get("login", "Admin\LoginController@index");
Route::get("log-out", "Admin\HomeController@log_out");
Route::post("login/proses", "Admin\LoginController@cek_login");

Route::group(["middleware" => ["CekSession"]], function() {
  Route::get("admin","Admin\MasterData\UserController@index");
  Route::group(["prefix" => "admin"], function() {
    Route::get("user","Admin\MasterData\UserController@index");
    Route::post("user/get-data","Admin\MasterData\UserController@get_data");
    Route::post("user/cek-email","Admin\MasterData\UserController@cek_email");
    Route::post("user/insert","Admin\MasterData\UserController@insert");
    Route::post("user/update","Admin\MasterData\UserController@update");
    Route::post("user/delete","Admin\MasterData\UserController@delete");
    Route::post("user/edit","Admin\MasterData\UserController@edit");
    // Route::get("user/mail", "Admin\MasterData\UserController@mail");

    Route::get("jenis-konten","Admin\MasterData\JenisKontenController@index");
    Route::post("jenis-konten/insert", "Admin\MasterData\JenisKontenController@insert");
    Route::post("jenis-konten/get-data", "Admin\MasterData\JenisKontenController@get_data");
    Route::post("jenis-konten/update", "Admin\MasterData\JenisKontenController@update");
    Route::post("jenis-konten/edit", "Admin\MasterData\JenisKontenController@edit");
    Route::post("jenis-konten/delete", "Admin\MasterData\JenisKontenController@delete");

    Route::get("konten", "Admin\MasterData\KontenController@index");
    Route::post("konten/get-data", "Admin\MasterData\KontenController@get_data");
    Route::post("konten/insert", "Admin\MasterData\KontenController@insert");
    Route::post("konten/delete", "Admin\MasterData\KontenController@delete");
    Route::post("konten/update", "Admin\MasterData\KontenController@update");
    Route::post("konten/edit", "Admin\MasterData\KontenController@edit");

    Route::get("header", "Admin\MasterData\HeaderController@index");
    Route::post("header/get-data-image", "Admin\MasterData\HeaderController@get_data_image");
    Route::post("header/insert-image", "Admin\MasterData\HeaderController@insert_image");
    Route::post("header/update-image", "Admin\MasterData\HeaderController@update_image");

    Route::get("tim", "Admin\MasterData\TimController@index");
    Route::post("tim/insert", "Admin\MasterData\TimController@insert");
    Route::post("tim/update", "Admin\MasterData\TimController@update");
    Route::post("tim/edit", "Admin\MasterData\TimController@edit");
    Route::post("tim/delete", "Admin\MasterData\TimController@delete");
    Route::post("tim/get-data", "Admin\MasterData\TimController@get_data");

    Route::get("portofolio","Admin\MasterData\PortofolioController@index");
    Route::get("portofolio/get-jenis-porto","Admin\MasterData\PortofolioController@get_jenis_porto");
    Route::get("portofolio/create","Admin\MasterData\PortofolioController@create");
    Route::get("portofolio/edit/{id}","Admin\MasterData\PortofolioController@edit");
    Route::get("portofolio/view/{id}", "Admin\MasterData\PortofolioController@view");
    Route::post("portofolio/get-data","Admin\MasterData\PortofolioController@get_data");
    Route::post("portofolio/insert","Admin\MasterData\PortofolioController@insert");
    Route::post("portofolio/update","Admin\MasterData\PortofolioController@update");
    Route::post("portofolio/delete","Admin\MasterData\PortofolioController@delete");

    Route::get("customer", "Admin\MasterData\CustomerController@index");
    Route::post("customer/insert", "Admin\MasterData\CustomerController@insert");
    Route::post("customer/edit", "Admin\MasterData\CustomerController@edit");
    Route::post("customer/update", "Admin\MasterData\CustomerController@update");
    Route::post("customer/delete", "Admin\MasterData\CustomerController@delete");
    Route::post("customer/get-data", "Admin\MasterData\CustomerController@get_data");

    Route::get("general", "Admin\MasterData\GeneralController@index");
    Route::post("general/load-email-config", "Admin\MasterData\GeneralController@load_email_config");
    Route::post("general/update-email-config", "Admin\MasterData\GeneralController@update_email_config");
    Route::post("general/update-general-config", "Admin\MasterData\GeneralController@update_general_config");
    Route::post("general/load-general-config", "Admin\MasterData\GeneralController@load_general_config");
    Route::post("general/load-file-download", "Admin\MasterData\GeneralController@load_file_download");
    Route::post("general/insert-file-download", "Admin\MasterData\GeneralController@insert_file_download");
    Route::post("general/edit-file-download", "Admin\MasterData\GeneralController@edit_file_download");
    Route::post("general/update-file-download", "Admin\MasterData\GeneralController@update_file_download");
    Route::post("general/delete-file-download", "Admin\MasterData\GeneralController@delete_file_download");
    Route::get("general/view-file-download/{id}", "Admin\MasterData\GeneralController@view_file_download");
  });
});