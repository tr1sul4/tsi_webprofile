@extends('admin.home')

@section('content')
<section class="content-header">
  <h1>User <small> Admin</small></h1>
  <ol class="breadcrumb">
    <li class="active"><a><i class="fa fa-folder"></i> User</a></li>
  </ol>
  <button class="btn btn-md btn-primary" id="btnAdd" ><i class="fa fa-plus"></i> Add</button>
</section>

<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <!-- <h3 class="box-title">Blank Box</h3> -->
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped display compact" id="table_data" width="100%">
        <thead>
          <tr>
            <th width="5px">No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Act.</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form class="form-horizontal" id="form" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" name="id" id="id" value="0">
          
          <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-9">
              <input type="email" class="form-control" id="email" name="email"></input>
              <div id="notif-email"></div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nama" name="nama"></input>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="password" name="password"></input>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="btnSave" data-loading-text='Save <i class="fa fa-spinner fa-spin"></i>'>Save</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </form>
    </div>
    
  </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {


    $("#table_data").DataTable({
      ajax: {
        url: "{{ url('admin/user/get-data') }}",
        type: "POST",
        dataType: "json",
      },
      columns:[
        {data: "no"},
        {data: "nm_user"},
        {data: "email"},
        {data: "act"},
      ],
    });
    
    $(document).on('click', '#btnAdd', function(event) {
      event.preventDefault();
      $("#modal-form").modal("show");
      rst_mdl();
      save_method = "insert";
    });

    $(document).on('change', '#email', function(event) {
      event.preventDefault();
      $("#notif-email").html("");
      var email = $("#email").val();
      $.ajax({
        url: "{{ url('admin/user/cek-email') }}",
        type: "POST",
        dataType: "json",
        data: {email: email, id: $("#id").val()},
        success: function (dt) {
          if (dt.code == 1) {
            $("#notif-email").html("<font color='red'>Email has been used!</font>");
            $("#email").val("");
            $("#email").attr("placeholder", email);
            $("#email").attr("style","background:#FAEDEC;border:1px solid #E85445");
          } else {
            $("#email").attr("style","");
          }
        },
      });
    });

    $(document).on('submit', '#form', function(event) {
      event.preventDefault();
      var nama = $("#nama").val();
      var email = $("#email").val();
      
        if (email.trim() == "") {
          alert("Empty email");
          $("#email").focus();
        } else if (nama.trim() == "") {
          alert("Empty name");
          $("#nama").focus();
        } else {
          if (save_method == "insert") {
            if ($("#password").val().trim() == "") {
              alert("Empty password");
              $("#password").focus();
            } else {
              post("{{ url('admin/user/insert') }}");
            }
          } else {
           post("{{ url('admin/user/update') }}");
          }

          function post(url) {
            $("#btnSave").prop("disabled", true);
            $("#btnSave").button("loading");
            $.ajax({
              url: url,
              type: 'POST',
              dataType: 'json',
              data: $("#form").serializeArray(),
            })
            .done(function(dt) {
              $("#table_data").DataTable().ajax.reload(null, false);
              $("#modal-form").modal("hide");
              rst_mdl();
              if (dt.code == 0) {
                global_notif_swal("success", dt.desc);
              } else {
                global_notif_swal("error", dt.desc);
              }
            })
            .fail(function(xhr, error, thrown) {
              console.log(error);
            });
          }
        }
    });

  });

  function rst_mdl()
  {
    $("#notif-email").html("");
    $("#email").attr("style","");
    $("#email").attr("placeholder", "");
    $("#form")[0].reset();
    $("#btnSave").prop("disabled", false);
    $("#btnSave").removeClass('disabled');
    $("#btnSave").button("reset");
  }
  function edit_data(id)
  {
    $.ajax({
      url: "{{ url('admin/user/edit') }}",
      type: "post",
      dataType: "json",
      data: {id:id},
      success: function (dt) {
        save_method = "update";
        $("#form")[0].reset();
        $("#id").val(dt.id_user);
        $("#nama").val(dt.nm_user);
        $("#email").val(dt.email);
        $("#modal-form").modal("show");
      },
      error: function (xhr, error, thrown) { console.log(error); },
    });
  }
  function delete_data(id)
  {
    if (confirm("Are you sure delete this data ?")) {
      $.ajax({
        url: "{{ url('admin/user/delete') }}",
        type: "post",
        dataType: "json",
        data: {id: id},
        success: function (dt) {
          global_notif_swal("success", dt.desc);
          $("#table_data").DataTable().ajax.reload();
          console.log(dt)
        },
        error: function (xhr, error, thrown) {
          console.log(error)
        },
      });
    }
  }
</script>
@endsection