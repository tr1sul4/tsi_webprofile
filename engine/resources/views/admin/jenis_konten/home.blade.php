@extends('admin.home')

@section('content')
<section class="content-header">
  <h1>Jenis Konten <small> Admin</small></h1>
  <ol class="breadcrumb">
    <li class="active"><a><i class="fa fa-folder"></i> Jenis Konten</a></li>
  </ol>
  <!-- <button class="btn btn-md btn-primary" id="btn-add"><i class="fa fa-plus"></i> Add</button> -->
</section>

<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <!-- <h3 class="box-title">Blank Box</h3> -->
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped display compact" id="table_data" width="100%">
        <thead>
          <tr>
            <th width="5px">No</th>
            <th>Nama jenis</th>
            <th>Ket.</th>
            <th width="45px">Act.</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>

<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form class="form-horizontal" id="form" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" name="id" id="id">
          <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nm_jenis_konten" name="nm_jenis_konten">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Ket.</label>
            <div class="col-sm-9">
              <textarea class="form-control" id="ket" name="ket" rows="5"></textarea>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="btnSave" data-loading-text='Save <i class="fa fa-spinner fa-spin"></i>'>Save</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </form>
    </div>
    
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $("#table_data").DataTable({
      ajax: {
        url: "{{ url('admin/jenis-konten/get-data') }}",
        type: "POST",
        dataType: "json",
      },
      columns:[
        {data: "no"},
        {data: "nm_jenis_konten"},
        {data: "ket"},
        {data: "act"},
      ],
    });
    $(document).on('click', '#btn-add', function(event) {
      event.preventDefault();
      $("#modal-form").modal("show");
      rst_mdl();
      save_method = "add";
    });

    $(document).on('submit', '#form', function(event) {
      event.preventDefault();
      var nm_jenis_konten = $("#nm_jenis_konten");
      var ket = $("#ket");
      if (nm_jenis_konten.val().trim() == "") {
        alert("Empty nama jenis konten");
        nm_jenis_konten.focus();
      } 
      // else if (ket.val().trim() == "") {
      //   alert("Empty ket.");
      //   ket.focus();
      // } 
      else {
        $("#btnSave").prop("disabled", true);
        $("#btnSave").button("loading");

        if (save_method == "add") {
          var url = "{{ url('admin/jenis-konten/insert') }}";
        } else {
          var url = "{{ url('admin/jenis-konten/update') }}";
        }

        $.ajax({
          url: url,
          type: 'POST',
          dataType: 'json',
          data: $("#form").serializeArray(),
        })
        .done(function(data) {
          // console.log(data);
          $("#modal-form").modal("hide");
          rst_mdl();
          $("#table_data").DataTable().ajax.reload(null, false);
          global_notif_swal("success", "Data berhasil disimpan");
        })
        .fail(function(xhr, error, thrown) {
          console.log(error);
          $("#modal-form").modal("hide");
          rst_mdl();
          global_notif_swal("error", error);
        });
      }
    });

  });

  function rst_mdl()
  {
    $("#form")[0].reset();
    $("#btnSave").prop("disabled", false);
    $("#btnSave").removeClass('disabled');
    $("#btnSave").button("reset");
  }

  function delete_data(id)
  {
    if (confirm("Are'u sure delete this data ?")) {
      $.ajax({
        url: "{{ url('admin/jenis-konten/delete') }}",
        type: "POST",
        dataType: "json",
        data: {id:id},
        success: function(data) {
          $("#table_data").DataTable().ajax.reload(null, false);
        },
        error: function(xhr, error, thrown) {
          console.log(error);
          global_notif_swal("error", error);
        },
      });
    }
  }

  function edit_data(id)
  {
    $.ajax({
      url: "{{ url('admin/jenis-konten/edit') }}",
      type: "POST",
      dataType: "json",
      data: {id:id},
      success: function(data) {
        save_method = "update";
        $("[name=\"id\"]").val(data.id_jenis_konten);
        $("[name=\"nm_jenis_konten\"]").val(data.nm_jenis_konten);
        $("[name=\"ket\"]").val(data.ket);
        $("#modal-form").modal("show");
      },
      error: function(xhr, error, thrown) {
        console.log(error);
        global_notif_swal("error", error);
      },
    });
  }
</script>
@endsection