@extends('admin.home')

@section('content')
<section class="content-header">
  <h1>Customer <small> Admin</small></h1>
  <ol class="breadcrumb">
    <li class="active"><a><i class="fa fa-folder"></i> Customer</a></li>
  </ol>
  <button class="btn btn-md btn-primary" id="btnAdd"><i class="fa fa-plus"></i> Add</button>
</section>

<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <!-- <h3 class="box-title">Blank Box</h3> -->
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped display compact" id="table_data" width="100%">
        <thead>
          <tr>
            <th width="5px">No</th>
            <th>Name</th>
            <th>URL</th>
            <th width="80px">Act.</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
  
  
</section>

<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" id="form" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" name="id" id="id">

          <div class="form-group">
            <label class="col-sm-2 control-label">Name </label>
            <div class="col-sm-10">
              <input class="form-control" id="nm_customer" name="nm_customer" type="text"></input>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Logo </label>
            <div class="col-sm-10">
              <input class="form-control" id="file" name="file" type="file" accept="image/*"></input>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">URL </label>
            <div class="col-sm-10">
              <input class="form-control" id="url" name="url" type="text"></input>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="btnSave" data-loading-text='Save <i class="fa fa-spinner fa-spin"></i>'>Save</button>
        </div>
      </form>
    </div>
    
  </div>
</div>


@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $("#table_data").DataTable({
      ajax: {
        url: "{{ url('admin/customer/get-data') }}",
        type: "POST",
        dataType: "json",
      },
      columns:[
        {data: "no"},
        {data: "nm_customer"},
        {data: "url"},
        {data: "act"},
      ],
    });

    $(document).on('click', '#btnAdd', function(event) {
      event.preventDefault();
      $("#modal-form").modal("show");
      save_method = "insert";
      rst_mdl();
    });

    $(document).on('submit', '#form', function(event) {
      event.preventDefault();
      if ($("#nm_customer").val().trim() == "") {
        alert("Empty Name");
        $("#nm_customer").focus();
      } else {
        $("#btnSave").prop("disabled", true);
        $("#btnSave").button("loading");
        if (save_method == "insert") {
          var url = "{{ url('admin/customer/insert') }}";
        } else {
          var url = "{{ url('admin/customer/update') }}";
        }
        $.ajax({
          url         : url,
          type        : "POST",
          dataType    : "json",
          processData : false,
          contentType : false,
          cache       : false,
          data        : new FormData($("#form")[0]),
          success: function (data) {
            $("#modal-form").modal("hide");
            rst_mdl();
            $("#table_data").DataTable().ajax.reload(null, false);
            global_notif_swal("success", data.desc);
          },
          error: function (xhr, error, thrown) {
            // console.log(error, xhr);
            if (xhr.status != 200) {
              $("#form")[0].reset();
              $("#modal-form").modal("hide");
              $("#btnSave").button("reset");
              $("#btnSave").prop('disabled', false);
              $("#btnSave").removeClass('disabled');
              var dt = JSON.parse(xhr.responseText).errors.file;
              console.log(dt);
              global_notif_swal("error", dt);
            } else {
              console.log(xhr);
              console.log(error);
            }
          },
        })
      }
    });

  });

function delete_data(id)
{
  if (confirm("Are'u sure delete this data ?")) {
    $.ajax({
      url: "{{ url('admin/customer/delete') }}",
      type: "POST",
      dataType: "json",
      success: function (data) {
        $("#table_data").DataTable().ajax.reload(null, false);
        global_notif_swal("success", data.desc);
      },
      error: function (xhr, error, thrown) {
        console.log(error);
      },
    });
  }
}

function edit_data(id)
{
  $.ajax({
    url: "{{ url('admin/customer/edit') }}",
    type: "POST",
    dataType: "json",
    data: {id:id},
    success: function (data) {
      rst_mdl();
      save_method = "update";
      $("[name=\"id\"]").val(data.id_customer);
      $("[name=\"nm_customer\"]").val(data.nm_customer);
      $("[name=\"url\"]").val(data.url);
      $("#modal-form").modal("show");
    },
    error: function (xhr, error, thrown) {},
  });
}

function rst_mdl()
{
  $("#form")[0].reset();
  $("#btnSave").prop("disabled", false);
  $("#btnSave").removeClass('disabled');
  $("#btnSave").button("reset");
}

</script>
@endsection