@extends('admin.home')

@section('content')
<section class="content-header">
  <h1>Team <small> Admin</small></h1>
  <ol class="breadcrumb">
    <li class="active"><a><i class="fa fa-folder"></i> Team</a></li>
  </ol>
  @if ($message = Session::get('success'))
  <div id="notif-alert" class="col-sm-offset-9 alert alert-success alert-dismissible" style="margin-bottom : 0"> 
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>
      <i class="icon fa fa-check"></i> Success</h4> {{ $message }}
    </div>
  @endif
  <button class="btn btn-md btn-primary" id="btnAdd"><i class="fa fa-plus"></i> Add</button>
</section>

<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <!-- <h3 class="box-title">Blank Box</h3> -->
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped display compact" id="table_data" width="100%">
        <thead>
          <tr>
            <th width="5px">No</th>
            <th>Name</th>
            <th>Position</th>
            <th>Note</th>
            <th>Act.</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->


</section>

<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form class="form-horizontal" id="form" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" name="id" id="id">

          <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-9">
              <input class="form-control" id="nm_tim" name="nm_tim"></input>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Position</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="jabatan" name="jabatan"></input>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Picture</label>
            <div class="col-sm-9">
              <input type="file" class="form-control" id="file" name="file"></input>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Note</label>
            <div class="col-sm-9">
              <textarea class="form-control" id="ket" name="ket" rows="4"></textarea>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="btnSave" data-loading-text='Save <i class="fa fa-spinner fa-spin"></i>'>Save</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </form>
    </div>
    
  </div>
</div>


@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $("#table_data").DataTable({
      responsive: true,
      ajax:{
        url: "{{ url('admin/tim/get-data') }}",
        type: "POST",
        dataType: "json",
      },
      columns:[
        {data: "no"},
        {data: "nm_tim"},
        {data: "jabatan"},
        {data: "ket"},
        {data: "act"},
      ]
    });

    $(document).on('click', '#btnAdd', function(event) {
      event.preventDefault();
      $("#modal-form").modal("show");
      rst_mdl();
      save_method = "insert";
    });

    $(document).on('submit', '#form', function(event) {
      event.preventDefault();
      var nm_tim = $("#nm_tim");
      var jabatan = $("#jabatan");
      if (nm_tim.val().trim() == "") {
        alert("Empty name");
        nm_tim.focus();
      } else if (jabatan.val().trim() == "") {
        alert("Empty position");
        jabatan.focus();
      } else {
        if (save_method == "insert") {
          var url = "{{ url('admin/tim/insert') }}";
        } else {
          var url = "{{ url('admin/tim/update') }}";
        }
        $("#btnSave").prop("disabled", false);
        $("#btnSave").button("loading");
        $.ajax({
          url: url,
          type: 'POST',
          dataType: 'json',
          processData : false,
          contentType : false,
          cache       : false,
          data: new FormData($("#form")[0]),
        })
        .done(function(dt) {
          $("#modal-form").modal("hide");
          rst_mdl();
          $("#table_data").DataTable().ajax.reload(null, false);
          if (dt.code == 0) {
            global_notif_swal("success", dt.desc);
          } else {
            global_notif_swal("error", dt.desc);
          }
        })
        .fail(function(xhr, error, thrown) {
          $("#modal-form").modal("hide");
          rst_mdl();
          if (xhr.status == 422) {
            var dt = JSON.parse(xhr.responseText).errors.file;
            console.log(dt);
            global_notif_swal("error", dt);
          } else {
            global_notif_swal("error", error);
          }
        });
      }
    });
  });
function rst_mdl()
{
  $("#form")[0].reset();
  $("#btnSave").prop("disabled", false);
  $("#btnSave").removeClass('disabled');
  $("#btnSave").button("reset");
}
function edit_data(id)
{
  $.ajax({
    url: "{{ url('admin/tim/edit') }}",
    type: "POST",
    dataType: "json",
    data: {id:id},
    success: function (dt) {
      rst_mdl();
      $("#id").val(dt.id_tim);
      $("#nm_tim").val(dt.nm_tim);
      $("#jabatan").val(dt.jabatan);
      $("#ket").val(dt.ket);
      $("#modal-form").modal("show");
      save_method = "update";
      // console.log(dt);
    },
    error: function (xhr, error, thrown) {
      console.log(error);
    },
  });
}
function delete_data(id)
{
  if (confirm("Are'u sure delete this data ?")) {
    $.ajax({
      url: "{{ url('admin/tim/delete') }}",
      type: "POST",
      dataType: "json",
      data: {id:id},
      success: function (dt) {
        console.log(dt);
        $("#table_data").DataTable().ajax.reload(null, false);
        global_notif_swal("success", dt.desc);
      },
      error: function (xhr, error, thrown) {
        console.log(xhr, error);
      }
    });
  }
}
</script>
@endsection