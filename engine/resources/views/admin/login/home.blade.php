<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<title>Trisula Indonesia</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/icon/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/icon/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/icon/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/icon/favicon-16x16.png') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
</head>
<body>
  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100 p-t-90 p-b-30">
        <form class="login100-form validate-form" id="form_login">
          <span class="login100-form-title p-b-40">Login</span>
          <div>
            
            <!-- <img src="{{ asset('images/icon/android-icon-192x192.png') }}" alt=""> -->
            <!-- <a href="#" class="btn-login-with bg1 m-b-10">
              <i class="fa fa-facebook-official"></i>Login with Facebook
            </a>
            <a href="#" class="btn-login-with bg2">
              <i class="fa fa-twitter"></i>Login with Twitter
            </a> -->
          </div>
          <!-- <div class="text-center p-t-55 p-b-30">
            <span class="txt1">Login with email</span>
          </div> -->
          <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter email: ex@abc.xyz">
            <input class="input100" type="text" name="user" id="user" placeholder="Email">
            <span class="focus-input100"></span>
          </div>
          <div class="wrap-input100 validate-input m-b-20" data-validate="Please enter password">
            <span class="btn-show-pass">
              <i class="fa fa fa-eye"></i>
            </span>
            <input class="input100" type="password" name="pass" id="pass" placeholder="Password">
            <span class="focus-input100"></span>
          </div>
          <div class="container-login100-form-btn">
            <button class="login100-form-btn" id="btn-submit">Login</button>
          </div>
        </form>
        <div class="flex-col-c p-t-224">
          <!-- <span class="txt2 p-b-10">Forgot?</span>
          <a href="#" class="txt3 bo1 hov1">Forgot account</a> -->
        </div>
      </div>
    </div>
  </div>

  <script src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendor/animsition/js/animsition.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendor/bootstrap/js/popper.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendor/select2/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendor/daterangepicker/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendor/countdowntime/countdowntime.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="5016ca16e84f4ec40b76c29c-text/javascript"></script>
  <!-- <script type="5016ca16e84f4ec40b76c29c-text/javascript">
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-23581568-13');
  </script> -->
  <!-- <script src="{{ asset('js/rocket-loader.min.js') }}" data-cf-settings="5016ca16e84f4ec40b76c29c-|49" defer=""></script> -->
  <script type="text/javascript">
    $(document).ready(function() {
      $(document).on('submit', '#form_login', function(event) {
        event.preventDefault();
        $("#btn-submit").prop("disabled", true);
        $.ajax({
          url: "{{ url('login/proses') }}",
          type: 'POST',
          dataType: 'json',
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          },
          data: {
            user: btoa($("#user").val()), 
            pass: btoa($("#pass").val())
          },
        })
        .done(function(dt) {
          if (dt.code == 1) {
            window.location.href = "{{ url('admin') }}";
            $("#pass").val("");
          } else {
            alert(dt.desc);
            $("#pass").val("");
          }
          $("#btn-submit").prop("disabled", false);
        })
        .fail(function(xhr, error, thrown) {
          console.log(error);
          alert(error)
          $("#btn-submit").prop("disabled", false);
        });

      });
    });
  </script>
</body>
</html>
