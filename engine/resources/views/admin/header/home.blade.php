@extends('admin.home')

@section('content')
<section class="content-header">
  <h1>Halaman Header <small> Admin</small></h1>
  <ol class="breadcrumb">
    <li class="active"><a><i class="fa fa-folder"></i> Halaman Header</a></li>
  </ol>
</section>

<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <!-- <h3 class="box-title">Blank Box</h3> -->
    </div>
    <div class="box-body">
      <button class="btn btn-md btn-primary" id="btnAdd"><i class="fa fa-plus"></i> Add</button><br><br>
      <table class="table table-bordered table-striped display compact" id="table_data" width="100%">
        <thead>
          <tr>
            <th width="5px">No</th>
            <th>Nama jenis</th>
            <th>Nama</th>
            <th>Ket.</th>
            <th>Act.</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  <div class="box box-default">
    <div class="box-header with-border">
      <h4 class="title-box">Image Header</h4>
    </div>
    <div class="box-body">
      <button class="btn btn-primary" id="btnAddImage"><i class="fa fa-plus"></i> Add</button>
      <table width="100%" id="table-image">
        <thead>
          <tr>
            <th>Nama</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>     
    </div>
  </div>
</section>

<div class="modal fade" id="modal-image" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" id="form-image" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" name="id" id="id">

          <div class="form-group">
            <label class="col-sm-2 control-label">File </label>
            <div class="col-sm-9">
              <input class="form-control" id="file" name="file" type="file"></input>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="btnSaveImage" data-loading-text='Save <i class="fa fa-spinner fa-spin"></i>'>Save</button>
        </div>
      </form>
    </div>
    
  </div>
</div>

<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form class="form-horizontal" id="form">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" name="id" id="id">

          <div class="form-group">
            <label class="col-sm-2 control-label">Nama jenis</label>
            <div class="col-sm-9">
              <select class="form-control" id="id_jenis_konten" name="id_jenis_konten"></select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nm_konten" name="nm_konten">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Ket.</label>
            <div class="col-sm-9">
              <textarea class="form-control" id="ket" name="ket" rows="5"></textarea>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="btnSave" data-loading-text='Save <i class="fa fa-spiner fa-spin"></i>'>Save</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </form>
    </div>
    
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $("#table_data").DataTable({
      // ajax: {
      //   url: "{{ url('admin/header/get-data') }}",
      //   type: "POST",
      //   dataType: "json",
      // },
      // columns:[
      //   {data: "no"},
      //   {data: "nm_jenis_konten"},
      //   {data: "nm_konten"},
      //   {data: "ket"},
      //   {data: "act"},
      // ],
    });
    // get_jenis_konten();
    $(document).on('click', '#btnAdd', function(event) {
      event.preventDefault();
      $("#modal-form").modal("show");
      save_method = "insert";
      rst_mdl();
      get_jenis_konten();
    });

    $(document).on('submit', '#form', function(event) {
      event.preventDefault();
      var nm_konten = $("#nm_konten");
      var ket       = $("#ket");
      if (nm_konten.val().trim() == "") {
        alert("Empty nama konten");
        nm_konten.focus();
      } else if (ket.val().trim() == "") {
        alert("Empty ket.");
        ket.focus();
      } else {
        $("#btnSave").prop("disabled", true);
        $("#btnSave").button("loading");
        if (save_method == "insert") {
          var url = "{{ url('admin/konten/insert') }}";
        } else {
          var url = "{{ url('admin/konten/update') }}";
        }
        $.ajax({
          url: url,
          type: "POST",
          dataType: "json",
          data: $("#form").serializeArray(),
          success: function (data) {
            console.log(data);
            $("#modal-form").modal("hide");
            $("#table_data").DataTable().ajax.reload(null, false);
            global_notif_swal("success", "data berhasil di insert");
          },
          error: function (xhr, error, thrown) {
            console.log(error);
            global_notif_swal("error", error);
          },
        });
      }
    });

    $(document).on('click', '#btnAddImage', function(event) {
      event.preventDefault();
      $("#modal-image").modal("show");
    });
    $(document).on('submit', '#form-image', function(event) {
      event.preventDefault();
      // console.log(new FormData($("#form-image")[0]) )
      if (document.getElementById("file").files.length == 0) {
        alert("Empty file");
        $("#file").focus();
      } else {
        $("#btnSaveImage").prop("disabled", true);
        $("#btnSaveImage").button("loading");
        $.ajax({
          url         : "{{ url('admin/header/insert-image') }}",
          type        : "POST",
          dataType    : "json",
          processData : false,
          contentType : false,
          cache       : false,
          data        : new FormData($("#form-image")[0]),
          success: function (data) {
            console.log(data);

          },
          error: function (xhr, error, thrown) {
            // console.log(error, xhr);
            if (xhr.status != 200) {
              $("#form-image")[0].reset();
              $("#modal-image").modal("hide");
              $("#btnSaveImage").button("reset");
              $("#btnSaveImage").prop('disabled', false);
              $("#btnSaveImage").removeClass('disabled');
              var dt = JSON.parse(xhr.responseText).errors.file;
              console.log(dt);
              global_notif_swal("error", dt);
            } else {
              console.log(xhr)
              console.log(error)
            }
          },
        })
      }
    });
  });
function delete_data(id)
{
  if (confirm("Are'u sure delete this data ?")) {
    $.ajax({
      url: "{{ url('admin/konten/delete') }}",
      type: "POST",
      dataType: "json",
      success: function (data) {
        console.log(data)
      },
      error: function (xhr, error, thrown) {
        console.log(error);
      },
    });
  }
}
function edit_data(id)
{
  $.ajax({
    url: "{{ url('admin/konten/edit') }}",
    type: "POST",
    dataType: "json",
    data: {id:id},
    success: function (data) {
      rst_mdl();
      save_method = "update";
      get_jenis_konten(data.id_jenis_konten);
      $("[name=\"id\"]").val(data.id_konten);
      $("[name=\"nm_konten\"]").val(data.nm_konten);
      $("[name=\"ket\"]").val(data.ket);
      $("#modal-form").modal("show");
    },
    error: function (xhr, error, thrown) {},
  });
}
function rst_mdl()
{
  $("#form")[0].reset();
  $("#btnSave").prop("disabled", false);
  $("#btnSave").removeClass('disabled');
  $("#btnSave").button("reset");
}

function get_jenis_konten(id = null)
{
  $.ajax({
    url: "{{ url('admin/jenis-konten/get-data') }}",
    type: "POST",
    dataType: "json",
    success: function(data) {
      $("#id_jenis_konten").html("");
      data.data.forEach((item) => {
        $("#id_jenis_konten").append(new Option(item.nm_jenis_konten, item.id_jenis_konten));
      })
      if (id != null) {
        $("#id_jenis_konten").val(id);
      }
    },
    error: function(xhr, error, thrown) {
      console.log(error);
    }
  });
}
</script>
@endsection