@extends('admin.home')

@section('content')
<section class="content-header">
  <h1>Portofolio <small> Admin</small></h1>
  <ol class="breadcrumb">
    <li class="active"><a><i class="fa fa-folder"></i> Portofolio</a></li>
  </ol>
  @if ($message = Session::get('success'))
  <div id="notif-alert" class="col-sm-offset-9 alert alert-success alert-dismissible" style="margin-bottom : 0"> 
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>
      <i class="icon fa fa-check"></i> Success</h4> {{ $message }}
    </div>
  @endif
  <a href="{{ url('admin/portofolio/create') }}" class="btn btn-md btn-primary" id="btnAdd"><i class="fa fa-plus"></i> Add</a>
</section>

<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <!-- <h3 class="box-title">Blank Box</h3> -->
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped display compact" id="table_data" width="100%">
        <thead>
          <tr>
            <th width="5px">No</th>
            <th>Type</th>
            <th>Name</th>
            <th>Act.</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->


</section>

@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $("#table_data").DataTable({
      responsive: true,
      ajax:{
        url: "{{ url('admin/portofolio/get-data') }}",
        type: "POST",
        dataType: "json",
      },
      columns:[
        {data: "no"},
        {data: "nm_jenis_portofolio"},
        {data: "nm_portofolio"},
        {data: "act"},
      ]
    });


  });
  function edit_data(id)
  {
    window.location.href = "{{ url('admin/portofolio/edit') }}/" + id;
  }
  function delete_data(id)
  {
    if (confirm("Are'u sure delete this data ?")) {
      $.ajax({
        url: "{{ url('admin/portofolio/delete') }}",
        type: "POST",
        dataType: "json",
        data:{id:id},
        success: function (dt) {
          $("#table_data").DataTable().ajax.reload(null, false);
          global_notif_swal("success", "Data saved success");
        },
        error: function (xhr, error, thrown) {console.log(xhr)},
      });
    }
  }
</script>
@endsection