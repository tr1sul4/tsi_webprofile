@extends('admin.home')

@section('content')
<section class="content-header">
  <h1>Create Portofolio <small> Admin</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('admin/portofolio') }}"><i class="fa fa-folder"></i> Portofolio</a></li>
    <li class="active"> Create Portofolio</li>
  </ol>
  @if ($errors->any())
    <div id="notif-alert" class="alert alert-danger col-sm-offset-7">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
</section>

<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <!-- <h3 class="box-title">Blank Box</h3> -->
    </div>
    <div class="box-body">
      
      <form class="form-horizontal" action="{{ url('admin/portofolio/insert') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
        <div class="box-body">
          @csrf
          <div class="form-group">
            <label class="col-sm-2 control-label">Type</label>
            <div class="col-sm-10">
              <select class="form-control" id="id_jenis_portofolio" name="id_jenis_portofolio"></select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="nm_portofolio" name="nm_portofolio">
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-10">
              <input class="form-control" id="file" name="file" type="file">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
              <textarea name="deskripsi" id="deskripsi" class="form-control summernote"></textarea>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-sm-11 text-right">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('admin/portofolio') }}" class="btn btn-default">Cancel</a>
          </div>
        </div>
        <!-- /.box-footer -->
      </form>

    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->


</section>




@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    get_jenis_porto();
  });
  
</script>
@include('admin.portofolio.funct')
@endsection