@extends('admin.home')

@section('content')
<section class="content-header">
  <h1>View Portofolio <small> Admin</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('admin/portofolio') }}"><i class="fa fa-folder"></i> Portofolio</a></li>
    <li class="active"> View Portofolio</li>
  </ol>
</section>

<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <!-- <h3 class="box-title">Blank Box</h3> -->
    </div>
    <div class="box-body">
      
      <form class="form-horizontal" >
        <div class="box-body">          
          <div class="form-group">
            <label class="col-sm-2 control-label">Type</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" value="{{ $jenis_portofolio['nm_jenis_portofolio'] }}">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" required="" class="form-control" id="nm_portofolio" name="nm_portofolio" value="{{ $nm_portofolio }}">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-10">
              <a <?= $berkas['nm_file_unik'] ? "href=\"".url('/berkas/'.$berkas['nm_file_unik'])."\" target=\"_blank\" style=\"color:#fff;background-color:#337ab7;\" " : "href=\"javascript:void(0)\" style=\"color:#fff;background-color:#990000;\" " ?>  class="btn btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
              <?= ($deskripsi != null) ? $deskripsi : "no description" ?>
            </div>
          </div>

        </div>
        <div class="box-footer">
          <div class="col-sm-11 text-right">
            <a href="{{ url('admin/portofolio') }}" type="button" class="btn btn-default">Back</a>
          </div>
        </div>
      </form>

    </div>
  </div>


</section>




@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    
  });
  
</script>
@endsection