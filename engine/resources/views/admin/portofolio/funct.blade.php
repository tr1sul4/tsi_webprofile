<script type="text/javascript">
  function get_jenis_porto(id = null)
  {
    $.ajax({
      url: "{{ url('admin/portofolio/get-jenis-porto') }}",
      type: "GET",
      dataType: "json",
      success: function(data) {
        $("#id_jenis_portofolio").html("");
        data.forEach((x) => {
          $("#id_jenis_portofolio").append(new Option(x.nm_jenis_portofolio, x.id_jenis_portofolio));
        });
        if (id != null) {
          $("#id_jenis_portofolio").val(id);
        }
        // console.log(data);
      },
      error: function(xhr, error, thrown) {
        console.log(xhr)
      },
    });
  }
</script>