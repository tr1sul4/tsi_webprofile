@extends('admin.home')

@section('content')
<section class="content-header">
  <h1>General <small> Admin</small></h1>
  <ol class="breadcrumb">
    <li class="active"><a><i class="fa fa-folder"></i> General</a></li>
  </ol>
  @if ($message = Session::get('success'))
  <div id="notif-alert" class="col-sm-offset-9 alert alert-success alert-dismissible" style="margin-bottom : 0"> 
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4>
      <i class="icon fa fa-check"></i> Success</h4> {{ $message }}
    </div>
  @endif
  <!-- <button class="btn btn-md btn-primary" id="btnAdd"><i class="fa fa-plus"></i> Add</button> -->
</section>

<section class="content">
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Email Confirguration</h3>
    </div>
    <div class="box-body">
      
      <form class="form-horizontal" id="form-email">
        <div class="box-body">
          <div class="form-group">
            <label class="col-sm-2 control-label">Name </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="name" name="name"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Host </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="host" name="host"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Username </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="username" name="username"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Password </label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="pass" name="pass"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Confirm Password </label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="repass" name="repass"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">SMTP Secure </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="smtp_secure" name="smtp_secure"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">SMPT Port </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="smtp_port" name="smtp_port"></input>
            </div>
          </div>

        </div>
        
        <div class="box-footer">
          <div class="col-sm-11 text-right">
            <button type="submit" class="btn btn-primary" id="btnUpdateEmail">Save</button>
          </div>
        </div>

      </form>

    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

  <div class="box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">General Config</h4>
    </div>
    <div class="box-body">
      <form class="form-horizontal" id="form-config">
        <div class="box-body">
          
          <div class="form-group">
            <label class="col-sm-2 control-label">Link Facebook </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="fb" name="fb"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Link Twitter </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="twitter" name="twitter"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Email </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="email" name="email"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Telp </label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="telp" name="telp"></input>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Address </label>
            <div class="col-sm-9">
              <textarea rows="6" class="form-control" id="alamat" name="alamat"></textarea>
            </div>
          </div>

        </div>
        <div class="box-footer">
          <div class="col-sm-11 text-right">
            <button type="submit" class="btn btn-primary" id="btnUpdateConfig">Save</button>
          </div>
        </div>

      </form>
    </div>
  </div>

  <div class="box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">File for send to customer</h4>
      <br>
      <button class="btn btn-md btn-primary" id="btnAddFile"><i class="fa fa-plus"></i> Add</button>
    </div>
    <div class="box-body">

      <table class=" compact" id="table table_file" width="100%">
        <thead>
          <tr>
            <th width="5px">No</th>
            <th>Name</th>
            <th>Ext</th>
            <th>Size</th>
            <th>Ket.</th>
            <th>Act</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>

    </div>
  </div>

</section>

<div class="modal fade" id="modal-file" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form class="form-horizontal" id="form-file" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" name="id_file" id="id_file">

          <div class="form-group">
            <label class="col-sm-2 control-label">File</label>
            <div class="col-sm-9">
              <input class="form-control" id="file" name="file" type="file"></input>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Note</label>
            <div class="col-sm-9">
              <textarea class="form-control" id="ket" name="ket" rows="4"></textarea>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="btnSaveFile" data-loading-text='Save <i class="fa fa-spinner fa-spin"></i>'>Save</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </form>
    </div>
    
  </div>
</div>


@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $("#table_file").DataTable({
      responsive: true,
      searching: false,
      paging: false,
      bInfo: false,
      ajax: {
        url: "{{ url('admin/general/load-file-download') }}", 
        type: "POST",
        dataType: "json",
      },
      columns:[
        {data: "no", orderable: false},
        {data: "nm_file_asli", orderable: false},
        {data: "ext", orderable: false},
        {data: "size", orderable: false},
        // {data: "mime", orderable: false},
        {data: "ket", orderable: false},
        {data: "act", orderable: false},
      ]
    });
    $('#form-email').bootstrapValidator({
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        name:{
          validators: {
            notEmpty: {
              message: "Name is required and can't be empty",
            },
          },
        },
        host: {
         validators: {
          notEmpty: {
            message: "Host is required and can't be empty",
          },
         },
        },
        username: {
          validators: {
            notEmpty: {
              message: "Username is required and can't be empty",
            },
          },
        },
        pass: {
          validators: {
            notEmpty: {
              message: "Password is required and can't be empty",
            },
            identical: {
                field: "repass",
              message: "Password and it's confirm are't same",
            },
          },
        },
        repass: {
          validators: {
            notEmpty: {
              message: "Confirm password is required and can't be empty",
            },
            identical: {
                field: "pass",
              message: "Password and it's confirm are't same",
            },
          },
        },
        smtp_secure: {
          validators: {
            notEmpty: {
              message: "SMTP Secure is required and can't be empty",
            },
          },
        },
        smtp_port: {
          validators: {
            notEmpty: {
              message: "SMTP Port is required and can't be empty",
            },
            integer: {
              message: 'The value is not an number'
            },
            stringLength: {
              min: 3,
              max: 3,
              message: "Value must 3 digit",
            },
          },
        },
      },
    })
    .on('success.form.bv', function(event) {
      event.preventDefault();
      if (confirm("Are you sure update this config ?")) {
        console.log($("#form-email").serializeArray());
        $.ajax({
          url: "{{ url('admin/general/update-email-config') }}",
          type: 'POST',
          dataType: 'json',
          data: $("#form-email").serializeArray(),
        })
        .done(function(data) {
          // console.log($("#form-email").serializeArray())
          global_notif_swal("success", data.desc);
          load_config_email();
        })
        .fail(function(xhr, error, thrown) {
          console.log(error);
        });
        
      } else {
        $("#btnUpdateEmail").prop("disabled", false);
        $("#btnUpdateEmail").removeClass('disabled');
      }
    });
    $(document).on('click', '#btnAddFile', function(event) {
      event.preventDefault();
      $("#modal-file").modal('show');
      $("#form-file")[0].reset();
      save_method = "insert";
    });
    $(document).on('submit', '#form-file', function(event) {
      event.preventDefault();
      if (save_method == "insert") {
        if (document.getElementById("file").files.length == 0) {
          alert("Empty file");
          $("#file").focus();
        } else {
          send_file("{{ url('admin/general/insert-file-download') }}");
        }
      } else {
        send_file("{{ url('admin/general/update-file-download') }}");
      }
      
      function send_file(url) {
        $("#btnSaveFile").prop("disabled", true);
        $("#btnSaveFile").button("loading");
        $.ajax({
          url: url,
          type: "POST",
          dataType: "json",
          processData : false,
          contentType : false,
          cache       : false,
          data: new FormData($("#form-file")[0]),
        })
        .done(function(dt) {
          // console.log(dt);
          $("#table_file").DataTable().ajax.reload(null, false);
          $("#modal-file").modal("hide");
          $("#btnSaveFile").prop("disabled", false);
          $("#btnSaveFile").removeClass("disabled");
          $("#btnSaveFile").button("rest");
          if (dt.code == 0) {
            global_notif_swal("success", dt.desc);
          } else {
            global_notif_swal("error", dt.desc);
          }
        })
        .fail(function(xhr, error, thrown) {
          console.log(error);
        });
      }
    }); 

    load_config_email(function(){
      // $('#form-email').bootstrapValidator('validate');
    });
    
    $(document).on('submit', '#form-config', function(event) {
      event.preventDefault();
      $('#form-config').bootstrapValidator();
    });

    $("#form-config").bootstrapValidator({
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        fb: {
          validators: {
            notEmpty:{
              message: "Link FB is required and can't be empty",
            },
            uri: {
              message: 'Link address is not valid',
            },
          },
        },
        twitter: {
          validators: {
            notEmpty:{
              message: "Link Twitter is required and can't be empty",
            },
            uri: {
              message: 'Link address is not valid',
            },
          },
        },
        email: {
          validators: {
            notEmpty:{
              message: "Email is required and can't be empty",
            },
            emailAddress: {
              message: 'The value is not a valid email address',
            },
          },
        },
        telp: {
          validators: {
            notEmpty:{
              message: "Telp is required and can't be empty",
            },
          },
        },
        alamat: {
          validators: {
            notEmpty:{
              message: "Address is required and can't be empty",
            },
          },
        },
      },
    }).on('success.form.bv', function(event) {
      event.preventDefault();
      if (confirm("Are you sure update this config ?")) {
        var data = $("#form-config").serializeArray();
        $.ajax({
          url: "{{ url('admin/general/update-general-config') }}",
          type: "POST",
          dataType: "json",
          data: data,
          success: function (dt) {
            global_notif_swal("success", dt.desc);
            console.log(dt)
            load_general_config();
          },
          error: function (xhr, error, thrown) { 
            console.log(error)
          },
        });
      } else {
        $("#btnUpdateConfig").prop("disabled", false);
        $("#btnUpdateConfig").removeClass('disabled');
      }
    });;
    load_general_config();
  });
function load_config_email()
{
  $.ajax({
    url: "{{ url('admin/general/load-email-config') }}",
    type: "POST",
    dataType: "json",
    success: function (dt) {
      $("#name").val(dt.name);
      $("#host").val(dt.host);
      $("#username").val(dt.username);
      $("#smtp_secure").val(dt.smtp_secure);
      $("#smtp_port").val(dt.smtp_port);
    },
    error: function (xhr, error, thrown) {
      console.log(error)
    },
  })
}

function load_general_config()
{
  $.ajax({
    url: "{{ url('admin/general/load-general-config') }}",
    type: "POST",
    dataType: "json",
    success: function (dt) {
      $("#fb").val(dt.fb);
      $("#twitter").val(dt.twitter);
      $("#email").val(dt.email);
      $("#telp").val(dt.telp);
      $("#alamat").val(dt.alamat);
    },
    error: function (xhr, error, thrown) {},
  });
}
function edit_data_file(id)
{
  $.ajax({
    url: "{{ url('admin/general/edit-file-download') }}",
    type: "POST",
    dataType: "json",
    data: {id:id},
    success: function (dt) {
      save_method = "update";
      $("#btnSaveFile").button("reset");
      $("#btnSaveFile").prop("disabled", false);
      $("#btnSaveFile").removeClass('disabled');
      $("#form-file")[0].reset();
      $("#id_file").val(dt.id_berkas);
      $("#ket").val(dt.keterangan);
      $("#modal-file").modal("show");
    },
    error: function (xhr, error, thrown) {
      console.log(error)
    },
  });
}
function delete_data_file(id)
{
  if (confirm("Are'u sure delete this data ?")) {
    $.ajax({
      url: "{{ url('admin/general/delete-file-download') }}",
      type: "POST",
      dataType: "json",
      data: {id:id},
      success: function (dt) {
        $("#table_file").DataTable().ajax.reload(null, false);
        global_notif_swal("success", dt.desc);
      },
      error: function (xhr, error, thrown) {
        console.log(error)
      },
    });
  }
}
</script>
@endsection