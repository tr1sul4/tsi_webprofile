@extends('dashboard.home')

@section('content')
<!-- banner -->
  <div class="banner1">
  </div>
<!-- //banner -->
<!---728x90--->

<!-- about -->
  <div class="about">
    <div class="container">
      <h3>Our Tim</h3>
      <!-- <p class="vel iste">Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</p> -->
      <div class="about-grids">
        <?php foreach($tim as $val) { ?>
          <div class="col-md-4 about-grid" style="margin: 10px 0px 0px 0px;">
            <div class="img-thumbnail" style="width: 350px; height: 350px; overflow: hidden; position: relative;">
              <img src="{{ url('/berkas/'.$val->image['nm_file_unik']) }}" alt=" " class="img-responsive" style="display: block; width: 100%;" />
            </div>
            <div class="about-grid-john">
              <div class="john">
                <h4 style="color: black;">{{$val->nm_tim}}</h4>
                <p>{{$val->jabatan}}</p>
              </div>
              <div class="clearfix"> </div>
              <!-- <p class="para">{{$val->ket}}</p> -->
                <br>
            </div>
          </div>
        <?php } ?>
       <!--  <div class="col-md-4 about-grid">
          <img src="images/10.jpg" alt=" " class="img-responsive" />
          <div class="about-grid-john">
            <div class="john">
              <h4>Mr.John Doe</h4>
              <p>Creative Director</p>
            </div>
            <div class="clearfix"> </div>
            <p class="para">Donec sollicitudin molestie male
                suada. Curabitur aliquet quam id dui 
                posuere blandit. Donec sollicitudin 
                molestie malesuada.</p><br>
          </div>
        </div>
        <div class="col-md-4 about-grid">
          <img src="images/12.jpg" alt=" " class="img-responsive" />
          <div class="about-grid-john">
            <div class="john">
              <h4>Miss.Laura Andrew</h4>
              <p>Creative Director</p>
            </div>
            <div class="clearfix"> </div>
            <p class="para">Donec sollicitudin molestie male
                suada. Curabitur aliquet quam id dui 
                posuere blandit. Donec sollicitudin 
                molestie malesuada.</p> <br>
          </div>
        </div> -->
        <div class="clearfix"> </div>
      </div>

<!---728x90--->

    </div>
  </div>

@endsection