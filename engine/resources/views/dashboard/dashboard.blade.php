@extends('dashboard.home')

@section('content')
<!-- banner -->
            
  <div class="banner">
    <div class="container">
      <section class="slider">
        <div class="flexslider">
          <ul class="slides">
            <?php
              foreach ($header->konten as $val) {
                echo "<li><div class=\"banner-info\"><h1>".$val->nm_konten . "</h1><p>" . $val->ket . "</p></div></li>";
              }
             ?>
          </ul>
        </div>
      </section>
    </div>
  </div>
<!-- //banner -->
<!---728x90--->

<!-- services -->
  <div class="services">
    <div class="container">
      <h3>{{ $service->nm_jenis_konten }}</h3>
      <p class="vel">{{ $service->ket }}</p>
      
      <div class="services-grids">
        <div class="col-md-6 services-grid-left">
          <div class="col-xs-4 services-grid-left1">
            <i class="hovicon effect-2 sub-a"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></i>
          </div>
          <div class="col-xs-8 services-grid-left2">
            <h4>{{ $service->konten[0]->nm_konten }}</h4>
            <p>{{ $service->konten[0]->ket }}</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="col-md-6 services-grid-right">
          <div class="col-xs-4 services-grid-left1">
            <i class="hovicon effect-2 sub-a"><span class="glyphicon glyphicon-grain" aria-hidden="true"></span></i>
          </div>
          <div class="col-xs-8 services-grid-left2">
            <h4>{{ $service->konten[1]->nm_konten }}</h4>
            <p>{{ $service->konten[1]->ket }}</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="services-grids">
        <div class="col-md-6 services-grid-left">
          <div class="col-xs-4 services-grid-left1">
            <i class="hovicon effect-2 sub-a"><span class="glyphicon glyphicon-signal" aria-hidden="true"></span></i>
          </div>
          <div class="col-xs-8 services-grid-left2">
            <h4>{{ $service->konten[2]->nm_konten }}</h4>
            <p>{{ $service->konten[2]->ket }}</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="col-md-6 services-grid-right">
          <div class="col-xs-4 services-grid-left1">
            <i class="hovicon effect-2 sub-a"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></i>
          </div>
          <div class="col-xs-8 services-grid-left2">
            <h4>{{ $service->konten[3]->nm_konten }}</h4>
            <p>{{ $service->konten[3]->ket }}</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      <!---728x90--->

    </div>
  </div>
<!-- //services -->
<!-- {{ $portofolio_all }} -->
<!-- portfolio -->
  <div id="portfolio" class="portfolio">
    <div class="container">
      <h3>{{ $work->nm_jenis_konten }}</h3>
      <p class="vel">{{ $work->ket }}</p>
      <div class="sap_tabs">      
        <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
          <ul class="resp-tabs-list">
            <li class="resp-tab-item"><span>All</span></li>
            <li class="resp-tab-item"><span>Web</span></li>
            <li class="resp-tab-item"><span>Mobile Apps</span></li>
            <li class="resp-tab-item"><span>Dekstop</span></li>         
          </ul> 
          <div class="clearfix"> </div> 
          <div class="resp-tabs-container">
            <div class="tab-1 resp-tab-content">                
              <div class="main">
                <?php foreach($portofolio_all as $val) { ?>
                <div class="view effect">
                  <div class="img-top">
                    <a href="{{ url('our-work/' . base64_encode($val->id_portofolio)) }}" target="_blank" rel="title" class="b-link-stripe b-animate-go  thickbox">
                      <img src="<?= url('/berkas/' . $val->berkas['nm_file_unik']) ?>" class="img-responsive center-block" alt="" style="display: block; width: 100%; margin: 0 auto;" />
                      <div class="mask"></div>
                      <div class="content">
                        <span class="info" title="Full Image"> </span>
                        <br><h4 style="color:#3AA079; font-weight: bold; -webkit-text-stroke:0.01px black;"><?= $val->nm_portofolio ?></h4>
                      </div>
                    </a>
                  </div>
                </div>
                <?php } ?>
                
                <div class="clearfix"> </div>
              </div>                                                               
            </div>
            <div class="tab-1 resp-tab-content">                
              <div class="main">

                <?php foreach($portofolio_web as $val) { ?>
                <div class="view effect">
                  <div class="img-top">
                    <a href="{{ url('our-work/' . base64_encode($val->id_portofolio)) }}" target="_blank" rel="title" class="b-link-stripe b-animate-go  thickbox">
                      <img src="<?= url('/berkas/' . $val->berkas['nm_file_unik']) ?>" class="img-responsive" alt="" style="display: block; width: 100%; margin: 0 auto;" />
                      <div class="mask"></div>
                      <div class="content">
                        <span class="info" title="Full Image"> </span>
                        <br><h4 style="color:#3AA079; font-weight: bold;"><?= $val->nm_portofolio ?></h4>
                      </div>
                    </a>
                  </div>
                </div>
                <?php } ?>
                
              </div>
            </div>
            <div class="tab-1 resp-tab-content">                
              <div class="main">
                <?php foreach($portofolio_mobile as $val) { ?>
                  <div class="view effect">
                    <div class="img-top">
                      <a href="{{ url('our-work/' . base64_encode($val->id_portofolio)) }}" target="_blank" rel="title" class="b-link-stripe b-animate-go  thickbox">
                        <img src="<?= url('/berkas/' . $val->berkas['nm_file_unik']) ?>" class="img-responsive" alt="" style="display: block; width: 100%; margin: 0 auto;" />
                        <div class="mask"></div>
                        <div class="content">
                          <span class="info" title="Full Image"> </span>
                          <br><h4 style="color:#3AA079; font-weight: bold;"><?= $val->nm_portofolio ?></h4>
                        </div>
                      </a>
                    </div>
                  </div>
                <?php } ?>
                

              </div>
            </div>
            <div class="tab-1 resp-tab-content">                
              <div class="main">
                <?php foreach($portofolio_dekstop as $val) { ?>
                  <div class="view effect">
                    <div class="img-top">
                      <a href="{{ url('our-work/' . base64_encode($val->id_portofolio)) }}" target="_blank" rel="title" class="b-link-stripe b-animate-go  thickbox">
                        <img src="<?= url('/berkas/' . $val->berkas['nm_file_unik']) ?>" class="img-responsive" alt="" style="display: block; width: 100%; margin: 0 auto;" />
                        <div class="mask"></div>
                        <div class="content">
                          <span class="info" title="Full Image"> </span>
                          <br><h4 style="color:#3AA079; font-weight: bold;"><?= $val->nm_portofolio ?></h4>
                        </div>
                      </a>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script src="{{ asset('js/easyResponsiveTabs.js') }}" type="text/javascript"></script>
      <script type="text/javascript">
        $(document).ready(function () {
          $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true   // 100% fit in a container
          });
        });
      </script>
    </div>
  </div>
<!-- //portfolio -->
<!-- features -->


  <div id="customer" class="typo">
    <div class="container">
      <h3 class="title">Our Customer</h3>
      <br><br>
          <div class="row">
            <center>
              <div class="col-md-12">
              <?php foreach ($customer as $val) { ?>
                <div class="col-md-3" style="padding: 20px 20px 20px 20px;">
                  <a href="{{ $val->url }}" target="_blank"><img src="{{ url('/berkas/' . $val->berkas->nm_file_unik) }}" alt="" style="max-height: 95px; max-width: 175px;"></a>
                  <br> 
                  <!-- <label style="padding: 10px 10px 10px 10px;">{{$val->nm_customer}}</label> -->
                </div>
              <?php } ?>                   
              </div>
            </center>
          </div>

    </div>
  </div>
<!-- //features -->

@endsection