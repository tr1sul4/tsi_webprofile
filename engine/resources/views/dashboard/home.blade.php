<!DOCTYPE html>
<html>
<head>
<title>Trisula Indonesia</title>

<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Trisula Indonesia, Pembuatan web, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns, Web design" />
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/icon/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/icon/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/icon/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/icon/favicon-16x16.png') }}">

<script type="application/x-javascript"> 
  addEventListener("load", function() { 
    setTimeout(hideURLbar, 0); 
  }, false);
  function hideURLbar(){ 
    window.scrollTo(0,1); 
  } 
</script>
<!-- //for-mobile-apps -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
<style type="text/css" media="screen">
  a{
    color: #4d0000;
  }
</style>
<!-- js -->
<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<!-- //js -->
<!--FlexSlider-->
    <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css" media="screen" />
    <script defer src="{{ asset('js/jquery.flexslider.js') }}"></script>
    <script type="text/javascript">
    $(window).load(function(){
      $('.flexslider').flexslider({
      animation: "slide",
      start: function(slider){
        $('body').removeClass('loading');
      }
      });
    });
    </script>
<!--End-slider-script-->
<!-- pop-up-script -->
<script src="{{ asset('js/jquery.chocolat.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/chocolat.css') }}" type="text/css" media="screen" charset="utf-8">
    <!--light-box-files -->
    <script type="text/javascript" charset="utf-8">
    $(function() {
      $('.img-top a').Chocolat();
    });
    </script>
<!-- //pop-up-script -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{ asset('js/move-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/easing.js') }}"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".scroll").click(function(event){   
      event.preventDefault();
      $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
    });
  });
</script>
<!-- start-smoth-scrolling -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="{{ asset('css/floating-wpp.min.css') }}">
</head>
  
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>


<!-- <meta name="robots" content="noindex"> -->
<body>
  <style type="text/css" media="screen">
    .banner{
      background:{{ url('/berkas') }}/{{ $background['nm_file_unik'] }} no-repeat 0px 0px !important;
    }
    .banner1{
      background:{{ url('/berkas') }}/{{ $background['nm_file_unik'] }} no-repeat 0px 0px !important;
    }
  </style>
<!-- header -->
  <div class="header-nav">
    <div class="container-fluid">
      <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>

          <div class="logo">
            <a class="navbar-brand" href="index.html"><img style="max-height: 90px;" src="{{ url('/images/logo_trisula.png') }}" alt=" "/></a>
          </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="act"><a href="{{ url('/') }}">Home</a></li>
            <li class="hvr-bubble-bottom"><a href="{{ url('our-tim') }}">Our Team</a></li>
            <li class="hvr-bubble-bottom"><a href="{{ url('/') }}#customer" class="scroll">Our Customer</a></li>
            <li class="hvr-bubble-bottom"><a href="{{ url('/') }}#portfolio" class="scroll">Portfolio</a></li>
            <!-- <li class="hvr-bubble-bottom"><a href="codes.html">Pages</a></li> -->
            <!-- <li class="hvr-bubble-bottom"><a href="blog.html">Blog</a></li> -->
            <li class="hvr-bubble-bottom"><a href="#contact" class="scroll">Contact Us</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->  
        
      </nav>
    </div>
  </div>
<!-- header -->
<!-- header-bottom -->
  <div class="header-bottom">
    <div class="container-fluid">
      <div class="header-bottom-right">
        <div class="header-bottom-right2">
          <ul>
            <li><a href="{{ $general_config['fb'] }}" class="facebook" target="_blank"> </a></li>
            
            <li><a href="{{ $general_config['twitter'] }}" class="twitter" target="_blank"> </a></li>
          </ul>
        </div>
        <div class="header-bottom-right1">
          <ul>
            <li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:{{ $general_config['email'] }}">{{ $general_config['email'] }}</a></li>
            <li><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>{{ $general_config['telp'] }}</li>
          </ul>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
<!-- //header-bottom -->
<!---728x90--->
@yield('content')
<!-- contact -->
  <div id="contact" class="contact">
    <div class="container">
      <div class="col-md-6 contact-left">
        <h2>Contact</h2>
        <br>
        <ul style="font-family: 'Oswald', sans-serif; font-weight: 300; ">
          <table style="color: white;">
            <tr>
              <td style="width: 35px;" valign="top"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></td>
              <td>{{ $general_config['alamat'] }}</td>
            </tr>
            <tr style="height: 50px">
              <td><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></td>
              <td>{{ $general_config['telp'] }}</td>
            </tr>
            <tr>
              <td><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></td>
              <td><a style="color: white;" href="mailto:{{ $general_config['email'] }}">{{ $general_config['email'] }}</a></td>
            </tr>
          </table>
        </ul>
      </div>
      <div class="col-md-6 contact-right">
        <div class="map" style="">
          <iframe src="https://maps.google.com/maps?q=-7.733522%2C%20110.388809&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border: 8px solid white;" allowfullscreen=""></iframe>
        </div>
      </div>
      <div class="clearfix"> </div>
      <div class="contact-bottom">
        <h2>Contact Us</h2>
        <form id="form_contact" autocomplete="off">
          <textarea id="note" name="note" type="text" placeholder="Message..." onfocus="this.value = '';" onblur="if (this.value == '') {}" required=""></textarea>
          <input type="text" id="nama" name="nama" placeholder="Name" onfocus="this.value = '';" onblur="if (this.value == '') {}" required="">
          <input type="email" id="email" name="email" placeholder="Email" onfocus="this.value = '';" onblur="if (this.value == ''){}" required="">
          <input type="submit" value="Submit" id="btnSubmit">
          <input type="reset" value="Clear" id="btnClear">
        </form>
      </div>
    </div>
  </div>
  <div class="floating-wpp"></div>
<!-- //contact -->
<!-- footer -->
  <div class="footer">
    <div class="container">
      <div class="footer-left">
        <p>&copy 2019 Trisula Indonesia, All rights reserved</p>
      </div>
      <div class="header-bottom-right2 footer-right">
        <ul>
          <li><a href="{{ $general_config['fb'] }}" class="facebook" target="_blank"> </a></li>
          <li><a href="{{ $general_config['twitter'] }}" class="twitter" target="_blank"> </a></li>
        </ul>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
<!-- //footer -->
<!-- here stars scrolling icon -->
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      
      $(document).on('submit', '#form_contact', function(event) {
        event.preventDefault();
        var data = $("#form_contact").serializeArray();
        $("#btnSubmit").prop("disabled", true);
        $("#btnClear").prop("disabled", true);
        $.ajax({
          url: "{{ url('kontak-kita') }}",
          type: "POST",
          dataType: "json",
          data: data,
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          },
          success: function (dt) {
            // console.log(dt);
            if (dt.code == 0) {
              $("#form_contact")[0].reset();
              Swal.fire({
                title: "Thanks for contact us.",
                html: "We send our company profile in your Email.<br><p>please cek your spam too.</p>",
                confirmButtonText: "ok",
              });
            } else {
              console.log(dt);
              $("#btnSubmit").prop("disabled", false);
              $("#btnSubmit").removeClass('disabled');
              $("#btnClear").prop("disabled", false);
              $("#btnClear").removeClass('disabled');
            }
            $("#btnSubmit").prop("disabled", false);
            $("#btnSubmit").removeClass('disabled');
            $("#btnClear").prop("disabled", false);
            $("#btnClear").removeClass('disabled');
          },
          error: function (xhr, error, thrown) {

          },
        });
      });

    });
  </script>
  <script src="{{ asset('js/bootstrap.js') }}"></script>
  <script src="{{ asset('js/floating-wpp.min.js') }}" type="text/javascript"></script>
  <script>
    $(function () {
      $('.floating-wpp').floatingWhatsApp({
        phone: '62895401232160',
        popupMessage: 'Welcome to Trisula Indonesia',
        showPopup: true,
        position: 'right',
        autoOpen: false,
        //autoOpenTimer: 4000,
        message: 'Halo trisula, saya ingin berkonsultasi.',
        //headerColor: 'orange',
        headerTitle: 'Whatsapp Message Box',
      });
    });
  </script>
</body>
</html>
