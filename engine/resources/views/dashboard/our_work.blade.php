@extends('dashboard.home')

@section('content')

  <div class="banner1">
  </div>
  <!-- //banner -->
  <!---728x90--->

  <!--typography-page -->
  <div class="typo">
    <div class="container">
      <h3 class="title">Our Work</h3>
      <!-- <br> -->
      <p class="vel iste" style="color: black;font-weight: bold; font-size: 30px;">{{ $portofolio->nm_portofolio }}</p>
      <div class="grid_3 grid_4">
        <?= $portofolio->deskripsi ?>
      </div>
    </div>
  </div>
@endsection