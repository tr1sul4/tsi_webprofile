<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portofolio extends Model
{
  protected $table = "portofolio";
  protected $primaryKey = "id_portofolio";
  public $timestamps = false;
  
  public function jenis_portofolio()
  {
    return $this->belongsTo("App\Models\JenisPortofolio", "id_jenis_portofolio");
  }

  public function berkas()
  {
    return $this->hasOne("App\Models\Berkas", "id_item")->where("nm_tabel", self::getTable());
  }
  
}
