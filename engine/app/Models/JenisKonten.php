<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisKonten extends Model
{
  protected $table = "jenis_konten";
  protected $primaryKey = "id_jenis_konten";
  public $timestamps = false;

  public function konten()
  {
    return $this->hasMany("App\Models\Konten", "id_jenis_konten");
  }
}
