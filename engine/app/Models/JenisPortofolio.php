<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisPortofolio extends Model
{
  protected $table = "jenis_portofolio";
  protected $primaryKey = "id_jenis_portofolio";
  public $timestamps = false;

}
