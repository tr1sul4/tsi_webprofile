<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  protected $table = "customer";
  protected $primaryKey = "id_customer";
  public $timestamps = false;

  public function berkas()
  {
    return $this->hasOne("App\Models\Berkas", "id_item")->where("nm_tabel", self::getTable());
  }
}
