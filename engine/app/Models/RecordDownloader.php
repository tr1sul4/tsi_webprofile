<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecordDownloader extends Model
{
  protected $table = "record_downloader";
  protected $primaryKey = "id_record";
  public $timestamps = false;
}
