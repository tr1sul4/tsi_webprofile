<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tim extends Model
{
  protected $table = "tim";
  protected $primaryKey = "id_tim";
  public $timestamps = false;

  public function image()
  {
    return $this->hasOne("App\Models\Berkas", "id_item")->where(["nm_tabel" => self::getTable()]);
  }

}
