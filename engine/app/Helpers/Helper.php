<?php
namespace App\Helpers;

use App\Models\Berkas;

class Helper {
  
  public static function buttonEditDelete($id, $param = null)
  {
    
    return "<div style=\"white-space: nowrap\">" . ($param != null ? $param : " ") . " <button onclick=\"edit_data(" . $id . ")\" class=\"btn btn-xs btn-default\"><i class=\"glyphicon glyphicon-pencil\"></i></button> <button onclick=\"delete_data(" . $id . ")\" class=\"btn btn-xs btn-danger\"><i class=\"glyphicon glyphicon-trash\"></i></button> </div>";
  }

  public static function buttonEdit($id)
  {
    return "<div style=\"white-space: nowrap\"> <button onclick=\"edit_data(" . $id . ")\" class=\"btn btn-xs btn-default\"><i class=\"glyphicon glyphicon-pencil\"></i></button> </div>"; 
  }

  public static function insertImage($file, $data)
  {
    try {
      $bks = Berkas::orderBy("id_berkas","desc")->first()->id_berkas;
      $nama_file_unik = md5($bks + 1) . "." . $file->getClientOriginalExtension();
      $berkas = new Berkas;
      $berkas->id_item     = $data->getKey();
      $berkas->id_uploader = session()->get("id_user");
      $berkas->nm_tabel    = $data->getTable();
      $berkas->nm_file_asli= $file->getClientOriginalName();
      $berkas->nm_file_unik= $nama_file_unik;
      $berkas->ext         = $file->getClientOriginalExtension();
      $berkas->size        = $file->getSize();
      $berkas->uploaded    = now()->ToDateTimeString();
      $berkas->save();

      $tujuan_upload = 'berkas/';
      $file->move($tujuan_upload, $nama_file_unik);
      return ["status" => true];
    } catch (Exception $e) {
      return ["status" => false, "desc" => $e];
    }
  }

  public static function updateImage($file, $data)
  {
    try {
      $cek = Berkas::where(["id_item" => $data->getKey(), "nm_tabel" => $data->getTable()])->first();
      if ($cek) {
        if (file_exists("berkas/" . $cek->nm_file_unik)) {
          unlink("berkas/" . $cek->nm_file_unik);
        }
        $nama_file_unik = md5($cek->id_berkas) . "." . $file->getClientOriginalExtension();
        $berkas = $cek;
      } else {
        $nama_file_unik = md5(Berkas::orderBy("id_berkas","desc")->first()->id_berkas + 1) . "." . $file->getClientOriginalExtension();
        $berkas = new Berkas;
      }
      $berkas->id_item     = $data->getKey();
      $berkas->id_uploader = session()->get("id_user");
      $berkas->nm_tabel    = $data->getTable();
      $berkas->nm_file_asli= $file->getClientOriginalName();
      $berkas->nm_file_unik= $nama_file_unik;
      $berkas->ext         = $file->getClientOriginalExtension();
      $berkas->size        = $file->getSize();
      if ($data) {
        $berkas->modified = now()->ToDateTimeString();
      } else {
        $berkas->uploaded = now()->ToDateTimeString();
      }
      $berkas->save();
      $tujuan_upload = 'berkas/';
      $file->move($tujuan_upload, $nama_file_unik);
      return ["status" => true, "desc" => "success"];
    } catch (Exception $e) {
      return ["status" => false, "desc" => $e];
    }
  }

  public static function generatePassword(){
    $length = 8;
    $str = "";
    $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
      $rand = mt_rand(0, $max);
      $str .= $characters[$rand];
    }
    return $str;
  }

}