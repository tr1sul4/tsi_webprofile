<?php
namespace App\Helpers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
use App\Helpers\Encrypt;
use DB;

class Mail {
  
  public static function send_mail($email_address, $nm_address, $subject = "We send you Email", $body = "")
  {
    $config = self::load_config();
    try {
      $mail = new PHPMailer(true);
      // $mail->SMTPDebug = 1;
      $mail->isSMTP();
      $mail->Host       = $config->host;
      $mail->SMTPAuth   = true;
      $mail->Username   = $config->username;
      $mail->Password   = Encrypt::decrypt($config->pass);
      $mail->SMTPSecure = $config->smtp_secure;
      $mail->Port       = $config->smtp_port;

      $mail->setFrom($config->username, $config->name);
      $mail->addAddress($email_address, $nm_address);

      $mail->isHTML(true);
      $mail->Subject = $subject;
      $mail->Body    = $body;      

      $mail->send();

      return ["status" => true, ];
    } catch (Exception $e) {
      // echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
      return ["status" => false, "desc" => $mail->ErrorInfo];
    }
  }

  public static function sendMailDownloader($email_address, $nm_address, $subject = "", $body = "", $attach = null)
  {
    try {
      $config = self::load_config();

      $mail = new PHPMailer(true);
      // $mail->SMTPDebug = 1;
      $mail->isSMTP();
      $mail->Host       = $config->host;
      $mail->SMTPAuth   = true;
      $mail->Username   = $config->username;
      $mail->Password   = Encrypt::decrypt($config->pass);
      $mail->SMTPSecure = $config->smtp_secure;
      $mail->Port       = $config->smtp_port;

      $mail->setFrom($config->username, $config->name);
      $mail->addAddress($email_address, $nm_address);

      if ($attach != null) {
        foreach ($attach as $val) {
          $mail->addAttachment(substr(base_path(), 0, -6) . "berkas\\" . $val->nm_file_unik, $val->nm_file_asli);
        }
      }
      // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');

      $mail->isHTML(true);
      $mail->Subject = $subject;
      $mail->Body    = $body;      

      $mail->send();
      return ["status" => true, ]; 
    } catch (Exception $e) {
      return ["status" => false, "desc" => $mail->ErrorInfo]; 
    }
  }

  public static function load_config()
  {
    $data = DB::table("global_config")->where(["variable" => "config", "profile"=>"email"])->select(["value"])->first()->value;
    return json_decode($data);
  }
}