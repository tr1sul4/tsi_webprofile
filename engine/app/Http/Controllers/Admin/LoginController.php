<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Encrypt;
class LoginController extends Controller
{
  public function index()
  {
    return view("admin.login.home");
  }
  public function cek_login(Request $req)
  {
    $email = base64_decode($req->user);
    $pass = base64_decode($req->pass);
    $dt = DB::table("user")->where("email", $email)->first();
    if ($dt) {
      $pswx = Encrypt::decrypt($dt->password);
      if ($pass == $pswx) {
        session()->put("id_user", $dt->id_user);
        session()->put("nm_user", $dt->nm_user);
        session()->put("email", $dt->email);
        return response()->json(["code" => 1, "desc" => "success"]);
      } else {
        return response()->json(["code" => 0, "desc" => "cek your creadential"]);  
      }
    } else {
      return response()->json(["code" => 0, "desc" => "cek your creadential"]);
    }
  }
}
