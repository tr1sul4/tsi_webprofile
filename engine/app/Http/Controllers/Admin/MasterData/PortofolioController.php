<?php

namespace App\Http\Controllers\Admin\MasterData;

use DB;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Models\Portofolio;

class PortofolioController extends Controller
{
  public function index()
  {
    return view("admin.portofolio.home");
  }
  public function create()
  {
    return view("admin.portofolio.create");
  }
  public function edit($id)
  {
    $data = Portofolio::find($id);
    return view("admin.portofolio.edit", $data);
  }
  public function view($id)
  {
    $data = Portofolio::with(["berkas","jenis_portofolio"])->find($id);
    return view("admin.portofolio.view", $data);
  }
  public function insert(Request $req)
  {
    if ($req->hasFile("file")) {
      $this->validate($req, [
        "file" => "required|file|image|mimes:jpeg,png,jpg",
      ]);
    }

    $porto = new Portofolio;
    $porto->id_jenis_portofolio = $req->id_jenis_portofolio;
    $porto->nm_portofolio       = $req->nm_portofolio;
    $porto->deskripsi           = $req->deskripsi;
    $porto->save();

    if ($req->hasFile("file")) {
      $berkas = Helper::updateImage($req->file("file"), $porto);
    }

    return redirect("admin/portofolio")->with("success", "Data Successfully Saved.");
  }
  public function get_jenis_porto(Request $req)
  {
    $data = DB::table("jenis_portofolio")->orderBy('nm_jenis_portofolio', 'asc')->get();
    return response()->json($data);
  }
  public function update(Request $req)
  {
    if ($req->hasFile("file")) {
      $this->validate($req, [
        "file" => "required|file|image|mimes:jpeg,png,jpg",
      ]);
    }
  
    $porto = Portofolio::find($req->id_portofolio);
    $porto->id_jenis_portofolio = $req->id_jenis_portofolio;
    $porto->nm_portofolio       = $req->nm_portofolio;
    $porto->deskripsi           = $req->deskripsi;
    $porto->save();

    if ($req->hasFile("file")) {
      $berkas = Helper::updateImage($req->file("file"), $porto);
    }
    return redirect("admin/portofolio")->with("success", "Data Successfully Update."); 
  }
  public function delete(Request $req)
  {
    Portofolio::where("id_portofolio", $req->id)->delete();
    return response()->json(["code"=>0, "desc"=>"success delete"]);
  }
  public function get_data()
  {
    $db = DB::table("portofolio as t1");
    $db->leftJoin("jenis_portofolio as t2","t1.id_jenis_portofolio","=","t2.id_jenis_portofolio");
    $db->select(["t1.id_portofolio", "t1.nm_portofolio", "t2.nm_jenis_portofolio"]);
    $data = [];
    $no = 1;
    foreach ($db->get() as $val) {
      $btn_view = " <a class=\"btn btn-xs bg-navy\" href=\"" . url('admin/portofolio/view/' . $val->id_portofolio) . "\"><i class=\"glyphicon glyphicon-eye-open\"></i></a> ";
      $data[]=[
        "no"                  => $no,
        "nm_jenis_portofolio" => $val->nm_jenis_portofolio,
        "nm_portofolio"       => $val->nm_portofolio,
        "act"                 => Helper::buttonEditDelete($val->id_portofolio, $btn_view),
      ];
      $no++;
    }
    return response()->json(["data" => $data]);
  }
}
