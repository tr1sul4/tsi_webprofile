<?php

namespace App\Http\Controllers\Admin\MasterData;

use DB;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JenisKontenController extends Controller
{

  public function index()
  {
    return view("admin.jenis_konten.home");  
  }

  public function get_data(Request $req)
  {
    $dt = DB::table("jenis_konten")->get();
    $data = [];
    $no = 1;
    foreach ($dt as $val) {
      $data[] = [
        "no"              => $no,
        "id_jenis_konten" => $val->id_jenis_konten,
        "nm_jenis_konten" => $val->nm_jenis_konten,
        "ket"             => $val->ket,
        "act"             => Helper::buttonEdit($val->id_jenis_konten),
      ];
      $no++;
    }
    return response()->json(["data" => $data]);
  }
  
  public function insert(Request $req)
  {
    $data = [
      "nm_jenis_konten" => $req->nm_jenis_konten,
      "ket"             => $req->ket,
    ];
    try {
      DB::table("jenis_konten")->insert($data);
      return response()->json(["code" => 0, "desc" => "success"]);
    } catch (Exception $e) {
      return response()->json(["code" => -1, "desc" => $e]);
    }
  }

  public function edit(Request $req)
  {
    $dt = DB::table("jenis_konten")->where("id_jenis_konten", $req->id)->first();
    return response()->json($dt);
  }

  public function update(Request $req)
  {
    $data = [
      "nm_jenis_konten" => $req->nm_jenis_konten,
      "ket"             => $req->ket,
    ];
    DB::table("jenis_konten")->where("id_jenis_konten", $req->id)->update($data);
    return response()->json(["code" => 0, "desc" => "success"]);
  }

  public function delete(Request $req)
  {
    DB::table("jenis_konten")->where("id_jenis_konten", $req->id)->delete();
    return response()->json(["code" => 0, "desc" => "success"]);
  }

}
