<?php

namespace App\Http\Controllers\Admin\MasterData;

use DB;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KontenController extends Controller
{

  public function index()
  {
    return view("admin.konten.home");
  }

  public function get_data(Request $req)
  {
    $db = DB::table("konten as t1");
    $db->leftJoin("jenis_konten as t2","t2.id_jenis_konten", "=", "t1.id_jenis_konten");
    $db->select(["t1.id_konten","t2.nm_jenis_konten","t1.nm_konten", "t1.ket"]);
    $data = [];
    $no = 1;
    foreach ($db->get() as $val) {
      $data[] = [
        "no" => $no,
        "nm_jenis_konten" => $val->nm_jenis_konten,
        "nm_konten" => $val->nm_konten,
        "ket" => $val->ket,
        "act" => Helper::buttonEdit($val->id_konten),
      ];
      $no++;
    }
    return response()->json(["data" => $data]);
  }
  
  public function insert(Request $req)
  {
    $data = [
      "id_jenis_konten" => $req->id_jenis_konten,
      "nm_konten"       => $req->nm_konten,
      "ket"             => $req->ket,
    ];
    DB::table("konten")->insert($data);
    return response()->json(["code" => 0, "desc" => "success"]);
  }

  public function edit(Request $req)
  {
    $dt = DB::table("konten")->where("id_konten", $req->id)->first();
    return response()->json($dt);
  }

  public function delete(Request $req)
  {

  }

  public function update(Request $req)
  {
    $data = [
      "id_jenis_konten" => $req->id_jenis_konten,
      "nm_konten"       => $req->nm_konten,
      "ket"             => $req->ket,
    ];
    DB::table("konten")->where("id_konten", $req->id)->update($data);
    return response()->json(["code" => 0, "desc" => "success"]);
  }

}
