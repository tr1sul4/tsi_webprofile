<?php

namespace App\Http\Controllers\Admin\MasterData;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HeaderController extends Controller
{
  public function index()
  {
    return view("admin.header.home");
  }
  // public function insert_image(Request $req)
  // {
  //   $this->validate($req, [
  //     "file" => "required|file|image|mimes:jpeg,png,jpg",
  //   ]);
    
  //   $file = $req->file('file');
     
  //   $nama_file = md5(1) . "." . $file->getClientOriginalExtension();
    
  //   // $tujuan_upload = 'berkas/';
  //   // $file->move($tujuan_upload,$nama_file);
  //   $data = [
  //     "id_item"     => 1,
  //     "id_uploader" => session()->get("id_user"),
  //     "nm_tabel"    => "image_header",
  //     "nm_file_asli"=> $file->getClientOriginalName(),
  //     "nm_file_unik"=> $nama_file,
  //     "ext"         => $file->getClientOriginalExtension(),
  //     "size"        => $file->getSize(),
  //     "uploaded"    => now()->ToDateTimeString()
  //   ];
  //   DB::table("berkas")->insert($data);
  //   return response()->json($data);
  // }
  public function update_image(Request $req)
  {
    $this->validate($req, [
      "file" => "required|file|image|mimes:jpeg,png,jpg",
    ]);
    $data_unlink = DB::table("berkas")->where("id_berkas",1)->first();
    if (file_exists("berkas/" . $data_unlink->nm_file_unik)) {
      unlink("berkas/" . $data_unlink->nm_file_unik);
    }

    $file = $req->file('file');
    $nama_file = md5(1) . ".jpg";
    // $nama_file = md5(1) . "." . $file->getClietOriginalExtension();
    $data = [
      "id_item"     => 1,
      "id_uploader" => session()->get("id_user"),
      "nm_tabel"    => "image_header",
      "nm_file_asli"=> $file->getClientOriginalName(),
      "nm_file_unik"=> $nama_file,
      "ext"         => $file->getClientOriginalExtension(),
      "size"        => $file->getSize(),
      "uploaded"    => now()->ToDateTimeString()
    ];

    $tujuan_upload = 'berkas/';
    $file->move($tujuan_upload, $nama_file);
    DB::table("berkas")->where("id_berkas", 1)->update($data);
    return response()->json($data);
  }
  public function get_data_image()
  {
    $dt = DB::table("berkas")->where("id_berkas", 1)->first();
    return response()->json($dt);
  }
  
}
