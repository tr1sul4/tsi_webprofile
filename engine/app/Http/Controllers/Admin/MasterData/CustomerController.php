<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Helpers\Helper;
use App\Models\Berkas;
use DB;

class CustomerController extends Controller
{
  
  public function index()
  {
    return view("admin.customer.home");
  }

  public function insert(Request $req)
  {
    if ($req->hasFile("file")) {
      $this->validate($req, [
        "file" => "required|file|image|mimes:jpeg,png,jpg",
      ]);
    }
    $cust = new Customer;
    $cust->nm_customer = $req->nm_customer;
    $cust->url         = $req->url;
    $cust->save();

    if ($req->hasFile("file")) {
      $berkas = Helper::insertImage($req->file("file"), $cust);
      if ($berkas["status"] == true) {
        return response()->json(["code" => 0, "desc" => "insert success"]);  
      } else {
        return response()->json(["code" => -1, "desc" => $berkas["desc"]]);
      }
    } else {
      return response()->json(["code" => 0, "desc" => "insert success"]);
    }

  }

  public function update(Request $req)
  {
    if ($req->hasFile("file")) {
      $this->validate($req, [
        "file" => "required|file|image|mimes:jpeg,png,jpg",
      ]);
    }
    $cust = Customer::find($req->id);
    if (empty($cust)) {
      return response()->json(["code" => -1, "desc" => "data not found"]);
    } else {
      $cust->nm_customer = $req->nm_customer;
      $cust->url = $req->url;
      $cust->save();
      if ($req->hasFile("file")) {
        $berkas = Helper::updateImage($req->file("file"), $cust);
        if ($berkas["status"] == true) {
          return response()->json(["code" => 0, "desc" => "update success "]);
        } else {
          return response()->json(["code" => -1, "desc" => $berkas["desc"]]);
        }
      } else {
        return response()->json(["code" => 0, "desc" => "saved success"]);
      }
    }
  }

  public function edit(Request $req)
  {
    return response()->json(Customer::find($req->id));
  }

  public function delete(Request $req)
  {
    $file = Berkas::where(["id_item" => $req->id,"nm_tabel" => "customer"])->first();
    if (file_exists("berkas/" . $file->nm_file_unik)) {
      unlink("berkas/" . $file->nm_file_unik);
    }
    Berkas::find($file->id_berkas)->delete();
    Customer::find($req->id)->delete();
    return response()->json(["code" => 0, "desc" => "delete success"]);
  }

  public function get_data()
  {
    $data = [];
    $no = 1;
    foreach (Customer::get() as $val) {
      $file = Berkas::where(["id_item" => $val->id_customer, "nm_tabel" => "customer"])->first()->nm_file_unik;
      $view = "<a href=\"" . url("/berkas/" . $file) . "\" target=\"_blank\" class=\"btn btn-xs bg-navy\"><i class=\"glyphicon glyphicon-eye-open\"></i></a>";
      $data[] = [
        "no"          => $no,
        "nm_customer" => $val->nm_customer,
        "url"         => $val->url,
        "act"         => Helper::buttonEditDelete($val->id_customer, $view),
      ];
      $no++;
    }
    return response()->json(["data" => $data]);
  }

}
