<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Models\Tim;
use App\Models\Berkas;
use Illuminate\Support\Facades\Storage;
use DB;

class TimController extends Controller
{
  public function index()
  {
    return view("admin.tim.home");
  }
  public function insert(Request $req)
  {
    if ($req->hasFile("file")) {
      $this->validate($req, [
        "file" => "required|file|image|mimes:jpeg,png,jpg",
      ]);
    }
    
    $tim = new Tim;
    $tim->nm_tim = $req->nm_tim;
    $tim->jabatan = $req->jabatan;
    $tim->ket = $req->ket;
    $tim->save();

    if ($req->hasFile("file")) {
      $berkas = Helper::insertImage($req->file("file"), $tim);
      if ($berkas["status"] == true) {
        return response()->json(["code" => 0, "desc" => "saved success"]);
      } else {
        return response()->json(["code" => -1, "desc" => $berkas["desc"]]);
      }
    } else {
      return response()->json(["code" => 0, "desc" => "saved success"]);
    }
    
  }
  public function update(Request $req)
  {
    if ($req->hasFile("file")) {
      $this->validate($req, [
        "file" => "required|file|image|mimes:jpeg,png,jpg",
      ]);
    }
    $tim = Tim::find($req->id);
    if (empty($tim)) {
      return response()->json(["code"=>-1,"desc" => "no data found"]);
    } else {
      $tim->nm_tim = $req->nm_tim;
      $tim->jabatan = $req->jabatan;
      $tim->ket = $req->ket;
      $tim->save();

      if ($req->hasFile("file")) {
        $berkas = Helper::updateImage($req->file("file"), $tim);
        if ($berkas["status"] == true) {
          return response()->json(["code" => 0, "desc" => "saved success"]);
        } else {
          return response()->json(["code" => -1, "desc" => $berkas["desc"]]);
        }
        
      } else {
        return response()->json(["code"=>0,"desc" => "saved success"]);
      }
    }
  }
  public function edit(Request $req)
  {
    return response()->json(Tim::find($req->id));
  }
  public function delete(Request $req)
  {
    $file = Berkas::where(["id_item" => $req->id,"nm_tabel" => "tim"])->first();
    if (file_exists("berkas/" . $file->nm_file_unik) == true) {
      unlink("berkas/" . $file->nm_file_unik);
    }
    Berkas::find($file->id_berkas)->delete();
    Tim::find($req->id)->delete();
    return response()->json(["code"=>0, "desc" => "delete success"]);
  }
  public function get_data(Request $req)
  {
    $db = DB::table("tim");
    $data = [];
    $no = 1;
    foreach ($db->get() as $val) {
      $file = Berkas::where(["id_item" => $val->id_tim, "nm_tabel" => "tim"])->first()->nm_file_unik;
      $view = "<a href=\"" . url("/berkas/" . $file) . "\" target=\"_blank\" class=\"btn btn-xs bg-navy\"><i class=\"glyphicon glyphicon-eye-open\"></i></a>";
      $data[]=[
        "no"      => $no,
        "nm_tim"  => $val->nm_tim,
        "jabatan" => $val->jabatan,
        "ket"     => $val->ket,
        "act"     => Helper::buttonEditDelete($val->id_tim, $view)
      ];
      $no++;
    }
    return response()->json(["data" => $data]);
  }

}
