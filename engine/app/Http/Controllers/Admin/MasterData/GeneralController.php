<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Encrypt;
use App\Helpers\Helper;
use App\Helpers\Mail;
use App\Models\Berkas;
use DB;

class GeneralController extends Controller
{
  public function index()
  {
    return view("admin.general.home");
  }
  
  public function email_config()
  {
    $config = [
      "name"        => "Admin Trisula Indonesia", 
      "host"        => "smtp.gmail.com",
      "username"    => "tsi.medsos@gmail.com",
      "pass"        => "k/vt6ZVh9AKIVgyhqIrZ4TNYnE2PO+OMS8iin/6w+X0fpR1vGhzMnxQwCMiBZNlcBrvNA82SfxTjSKS8BeFIAQ==",
      "smtp_secure" => "TLS",
      "smtp_port"   => "465",
    ];
  }

  public function insert_email_config(Request $req)
  {

  }

  public function update_email_config(Request $req)
  {
    $data = [ 
      "name"        => $req->name,
      "host"        => $req->host,
      "username"    => $req->username,
      "pass"        => Encrypt::encrypt($req->pass),
      "smtp_port"   => $req->smtp_port,
      "smtp_secure" => $req->smtp_secure,
    ];
    DB::table("global_config")->where(["variable"=>"config", "profile" => "email"])->update(["value" => json_encode($data)]);
    return response()->json(["code" => 0, "desc" => "update success",])->setEncodingOptions(JSON_PRETTY_PRINT);
  }

  public function load_email_config()
  {
    $data = Mail::load_config();
    return response()->json($data)->setEncodingOptions(JSON_PRETTY_PRINT);
  }
  
  public function update_general_config(Request $req)
  {
    DB::table("global_config")->where(["variable" => "sosmed", "profile" => "twitter"])->update(["value" => $req->twitter]);
    DB::table("global_config")->where(["variable" => "sosmed", "profile" => "facebook"])->update(["value" => $req->fb]);
    DB::table("global_config")->where(["variable" => "office", "profile" => "telp"])->update(["value" => $req->telp]);
    DB::table("global_config")->where(["variable" => "office", "profile" => "email"])->update(["value" => $req->email]);
    DB::table("global_config")->where(["variable" => "office", "profile" => "alamat"])->update(["value" => $req->alamat]);
    return response()->json(["code" => 0, "desc" => "update success"]);
  }

  public function load_general_config()
  {
    $data = [
      "twitter"=> DB::table("global_config")->where(["variable" => "sosmed", "profile" => "twitter"])->select(["value"])->first()->value,
      "fb"     => DB::table("global_config")->where(["variable" => "sosmed", "profile" => "facebook"])->select(["value"])->first()->value,
      "telp"   => DB::table("global_config")->where(["variable" => "office", "profile" => "telp"])->select(["value"])->first()->value,
      "email"  => DB::table("global_config")->where(["variable" => "office", "profile" => "email"])->select(["value"])->first()->value,
      "alamat" => DB::table("global_config")->where(["variable" => "office", "profile" => "alamat"])->select(["value"])->first()->value,
    ];
    return response()->json($data)->setEncodingOptions(JSON_PRETTY_PRINT);
  }

  public function load_file_download(Request $req)
  {
    $berkas = Berkas::where(["id_item" => 1, "nm_tabel" => "file_berkas"])->get();
    $data = [];
    $no = 1;
    foreach ($berkas as $val) {
      $info = "<a href=\"" . url("admin/general/view-file-download/" . base64_encode($val->id_berkas)) . "\" target=\"_blank\" class=\"btn btn-xs bg-navy\"><i class=\"glyphicon glyphicon-eye-open\"></i></a>";
      $data[] = [
        "no"           => $no, 
        "nm_file_asli" => $val->nm_file_asli . $val->id_berkas,
        "ext"          => $val->ext,
        "size"         => $val->size,
        // "mime"         =>  mime_content_type("berkas/".$val->nm_file_unik),
        "ket"          => $val->keterangan,
        "act"          => "<div style=\"white-space: nowrap\"> " . $info . " <button onclick=\"edit_data_file(" . $val->id_berkas . ")\" class=\"btn btn-xs btn-default\"><i class=\"glyphicon glyphicon-pencil\"></i></button> <button onclick=\"delete_data_file(" . $val->id_berkas . ")\" class=\"btn btn-xs btn-danger\"><i class=\"glyphicon glyphicon-trash\"></i></button> </div>"
      ];
      $no++;
    }
    return response()->json(["data" => $data]);
  }

  public function insert_file_download(Request $req)
  {
    try {
      $file = $req->file("file");
      $bks = Berkas::orderBy("id_berkas","desc")->first()->id_berkas;
      $nama_file_unik = md5($bks + 1) . "." . $file->getClientOriginalExtension();
      $berkas = new Berkas;
      $berkas->id_item     = 1;
      $berkas->id_uploader = session()->get("id_user");
      $berkas->nm_tabel    = "file_berkas";
      $berkas->nm_file_asli= $file->getClientOriginalName();
      $berkas->nm_file_unik= $nama_file_unik;
      $berkas->ext         = $file->getClientOriginalExtension();
      $berkas->size        = $file->getSize();
      $berkas->uploaded    = now()->ToDateTimeString();
      $berkas->keterangan = $req->ket;
      $berkas->save();

      $tujuan_upload = 'berkas/';
      $file->move($tujuan_upload, $nama_file_unik);
      return response()->json(["code" => 0, "desc" => "insert success"]);
    } catch (Exception $e) {
      return response()->json(["code" => -1, "desc" => $e]);
    }
  }
  public function edit_file_download(Request $req)
  {
    return response()->json(Berkas::find($req->id))->setEncodingOptions(JSON_PRETTY_PRINT);
  }
  public function update_file_download(Request $req)
  {
    if ($req->hasFile("file")) {
      try {
        $file = $req->file("file");
        $bks = Berkas::find($req->id_file);
        if (file_exists("berkas/" . $bks->nm_file_unik)) {
          unlink("berkas/" . $bks->nm_file_unik);
        }
        $nama_file_unik = md5($bks->id_berkas) . "." . $file->getClientOriginalExtension();
        $bks->id_item = 1;
        $bks->id_uploader = session()->get("id_user");
        $bks->nm_tabel = "file_berkas";
        $bks->nm_file_asli = $file->getClientOriginalName();
        $bks->nm_file_unik = $nama_file_unik;
        $bks->ext = $file->getClientOriginalExtension();
        $bks->size = $file->getSize();
        $bks->modified = now()->ToDateTimeString();
        $bks->keterangan = $req->ket;
        $bks->save();
        $tujuan_upload = "berkas/";
        $file->move($tujuan_upload, $nama_file_unik);
        return response()->json(["code" => 0, "desc" => "update success"]);
      } catch (Exception $e) {
        return response()->json(["code" => -1, "desc" => $e]);
      }
    } else {
      $bks = Berkas::find($req->id_file);
      $bks->keterangan = $req->ket;
      $bks->save();
      return response()->json(["code" => 0, "desc" => "update success"]);
    }
  }
  public function delete_file_download(Request $req)
  {
    $berkas = Berkas::find($req->id);
    if (file_exists("berkas/" . $berkas->nm_file_unik)) {
      unlink("berkas/" . $berkas->nm_file_unik);
    }
    $berkas->delete();
    return response()->json(["code" => 0, "desc" => "delete success"]);
  }
  
  public function view_file_download($id)
  {
    $berkas = Berkas::find(base64_decode($id));
    return response()->file("berkas/" . $berkas->nm_file_unik);
  }

}
