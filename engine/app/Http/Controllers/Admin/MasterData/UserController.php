<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Helpers\Helper;
use App\Helpers\Encrypt;
use App\Helpers\Mail;
use DB;


class UserController extends Controller
{
  
  public function index()
  {
    return view("admin.user.home");
  }
  
  public function get_data(Request $req)
  {
    $db = DB::table("user")->get();
    $data = [];
    $no = 1;
    foreach ($db as $val) {
      $edit = "<button onclick=\"edit_data(" . $val->id_user . ")\" class=\"btn btn-xs btn-default\"><i class=\"glyphicon glyphicon-pencil\"></i></button> ";
      
      if (session()->get("id_user") == $val->id_user) {
        $del = " ";
      } else {
        $del = " <button onclick=\"delete_data(" . $val->id_user . ")\" class=\"btn btn-xs btn-danger\"><i class=\"glyphicon glyphicon-trash\"></i></button>";
      }
      
      $data[] = [
        "no" => $no,
        "nm_user" => $val->nm_user,
        "email" => $val->email,
        "act" => "<div style=\"white-space: nowrap\"> " . $edit . $del . "</div>",
      ];
      $no++;
    }
    echo json_encode(["data" => $data,], JSON_PRETTY_PRINT);
  }
  public function cek_email(Request $req)
  {
    $cek = User::where(["email" => $req->email, ["id_user", "<>", $req->id]])->first();
    if ($cek) {
      return response()->json(["code" => 1, "desc" => "email registered"]);
    } else {
      return response()->json(["code" => 0, "desc" => "email not found"]);
    }
  }
  public function insert(Request $req)
  {
    // $pass = Helper::generatePassword();
    $passEncrypt = Encrypt::encrypt($req->password);
    // $subject = "Password receive";
    // $body = "your password admin trisula = <b>" . $gntPass . "</b>";
    // $mail = Mail::send_mail($req->email, $req->nama, $subject, $body);

    $dt = new User;
    $dt->nm_user = $req->nama;
    $dt->email = $req->email;
    $dt->password = $passEncrypt;
    $dt->save();
    return response()->json(["code" => 0, "desc" => "saved success"]);

    // if ($mail["status"] == true) {
    // } else {
    //   return response()->json(["code" => -1, "desc" => $mail]);
    // }

  }
  public function update(Request $req)
  {
    $dt = User::find($req->id);
    if ($req->password != "") {
      $passEncrypt = Encrypt::encrypt($req->password);
      $dt->password = $passEncrypt;
    }
    $dt->nm_user = $req->nama;
    $dt->email = $req->email;
    $dt->save();
    return response()->json(["code" => 0, "desc" => "update success"]);
  }
  
  public function edit(Request $req)
  {
    return response()->json(User::find($req->id));
  }
  public function delete(Request $req)
  {
    User::where("id_user", $req->id)->delete();
    return response()->json(["code" => 0, 'desc' => "delete success"])->setEncodingOptions(JSON_PRETTY_PRINT);
  }

}
