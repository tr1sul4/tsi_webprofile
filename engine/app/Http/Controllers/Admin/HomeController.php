<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;


class HomeController extends Controller
{
  public function index()
  {
    return view("admin.user.home");
  }

  public function log_out()
  {
    session()->flush();
    return redirect(url("admin"));
  }
}
