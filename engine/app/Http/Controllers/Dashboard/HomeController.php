<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Encrypt;
use App\Helpers\Mail;
use App\Models\JenisKonten;
use App\Models\Portofolio;
use App\Models\Berkas;
use App\Models\Customer;
use App\Models\Tim;
use App\Models\RecordDownloader;
use DB;

class HomeController extends Controller
{
  
  public function index()
  {
    $data["general_config"] = [
      "twitter"=> DB::table("global_config")->where(["variable" => "sosmed", "profile" => "twitter"])->select(["value"])->first()->value,
      "fb"     => DB::table("global_config")->where(["variable" => "sosmed", "profile" => "facebook"])->select(["value"])->first()->value,
      "telp"   => DB::table("global_config")->where(["variable" => "office", "profile" => "telp"])->select(["value"])->first()->value,
      "email"  => DB::table("global_config")->where(["variable" => "office", "profile" => "email"])->select(["value"])->first()->value,
      "alamat" => DB::table("global_config")->where(["variable" => "office", "profile" => "alamat"])->select(["value"])->first()->value,
    ];
    $data["portofolio_all"] = 
      Portofolio::with(["berkas" => function($x) { 
        $x->select(["id_item", "nm_tabel",  "nm_file_unik"]); 
      }])->select(["id_portofolio","nm_portofolio"])->orderBy(DB::raw("rand()"))->limit(6)->get();

    $data["portofolio_web"] = 
      Portofolio::with(["berkas" => function($x) {
        $x->select(["id_item", "nm_tabel", "nm_file_unik"]); 
      }])->where("id_jenis_portofolio", 1)->select(["id_portofolio", "nm_portofolio"])->get();

    $data["portofolio_mobile"] = 
      Portofolio::with(["berkas" => function($x) {
        $x->select(["id_item", "nm_tabel", "nm_file_unik"]); 
      }])->where("id_jenis_portofolio", 2)->select(["id_portofolio", "nm_portofolio"])->get();

    $data["portofolio_dekstop"] = 
      Portofolio::with(["berkas" => function($x) {
        $x->select(["id_item", "nm_tabel", "nm_file_unik"]); 
      }])->where("id_jenis_portofolio", 3)->select(["id_portofolio", "nm_portofolio"])->get();
    
    $data["customer"] = Customer::with(["berkas"])->get();

    $data["header"] = JenisKonten::with(["konten"])->where("id_jenis_konten", 4)->first();
    $data["service"] = JenisKonten::with(["konten"])->where("id_jenis_konten", 1)->first();
    $data["work"] = JenisKonten::with(["konten"])->where("id_jenis_konten", 2)->first();

    $data["background"] = Berkas::find(1);
    // $data["fitur"] = JenisKonten::with(["konten"])->where("id_jenis_konten", 3)->first();
    // return response()->json($data)->setEncodingOptions(JSON_PRETTY_PRINT);
    return view("dashboard.dashboard", $data);
    // echo json_encode($data, JSON_PRETTY_PRINT);
  }
  public function our_tim(Request $req)
  {
    $data["general_config"] = [
      "twitter"=> DB::table("global_config")->where(["variable" => "sosmed", "profile" => "twitter"])->select(["value"])->first()->value,
      "fb"     => DB::table("global_config")->where(["variable" => "sosmed", "profile" => "facebook"])->select(["value"])->first()->value,
      "telp"   => DB::table("global_config")->where(["variable" => "office", "profile" => "telp"])->select(["value"])->first()->value,
      "email"  => DB::table("global_config")->where(["variable" => "office", "profile" => "email"])->select(["value"])->first()->value,
      "alamat" => DB::table("global_config")->where(["variable" => "office", "profile" => "alamat"])->select(["value"])->first()->value,
    ];
    $data["tim"] = Tim::with(["image"])->get();
    $data["background"] = Berkas::find(1);
    return view("dashboard.our_tim", $data);
  }
  public function view_portofolio(Request $req)
  {

  }
  public function our_work(Request $req, $id)
  {
    $data["general_config"] = [
      "twitter"=> DB::table("global_config")->where(["variable" => "sosmed", "profile" => "twitter"])->select(["value"])->first()->value,
      "fb"     => DB::table("global_config")->where(["variable" => "sosmed", "profile" => "facebook"])->select(["value"])->first()->value,
      "telp"   => DB::table("global_config")->where(["variable" => "office", "profile" => "telp"])->select(["value"])->first()->value,
      "email"  => DB::table("global_config")->where(["variable" => "office", "profile" => "email"])->select(["value"])->first()->value,
      "alamat" => DB::table("global_config")->where(["variable" => "office", "profile" => "alamat"])->select(["value"])->first()->value,
    ];
    $data["portofolio"] = Portofolio::find(base64_decode($id));
    $data["background"] = Berkas::find(1);
    return view("dashboard.our_work", $data); 
  }
  
  public function contact_us(Request $req)
  {
    $dt = new RecordDownloader;
    $dt->nama = $req->nama;
    $dt->keterangan = $req->note;
    $dt->email = $req->email;
    $dt->date_record = now()->toDateTimeString();
    $dt->save();
    $berkas = Berkas::where(["id_item" => 1, "nm_tabel" => "file_berkas"])->get();
    // $data = [];
    // foreach ($berkas as $val) {
    //   $data[] = [
    //     "nm_file_unik" => $val->nm_file_unik,
    //     "nm_file_asli" => $val->nm_file_asli,
    //     "path"         => url("/berkas/" . $val->nm_file_unik),
    //   ];
    // }
    $subject = "Profile Trisula Indonesia";
    $body = "We send our profile company";
    $mail = Mail::sendMailDownloader($req->email, $req->nama, $subject, $body, $berkas);
    if ($mail["status"] == true) {
      echo json_encode(["code" => 0, "desc" => "insert success"]);
    } else {
      echo json_encode(["code" => -1, "desc" => $mail]);
    }
  }

  public function cek_berkas()
  {
    $berkas = Berkas::where(["id_item" => 1, "nm_tabel" => "file_berkas"])->get();
    $data = [];
    foreach ($berkas as $val) {
      $data[] = [
        "nm_file_unik" => $val->nm_file_unik,
        "nm_file_asli" => $val->nm_file_asli,
        "url"          => url("/berkas/" . $val->nm_file_unik),
        "public_path"  => public_path("/berkas/" . $val->nm_file_unik),
        "storage"     => storage_path("/berkas/" . $val->nm_file_unik),
        "lain"        => substr(base_path(), 0, -6),
        "app_path"    => app_path(),
        "base_path"   => base_path(),
        "asset"       => public_path(),
      ];
    }
    print_r($data);
    // return response()->json($data)->setEncodingOptions(JSON_PRETTY_PRINT);
  }

}
