create table global_config(
  variable varchar(50) not null default "",
  profile varchar(50) not null default "",
  value   text
)engine=MyISAM;

create table profil(
  id_profil int(1) unsigned auto_increment,
  nm_profil varchar(65) not null default "",
  deskripsi mediumtext,
  primary key(id_profil)
)engine=MyISAM;

create table user(
  id_user       int(1) unsigned auto_increment,
  nm_user       varchar(75) not null default "",
  email         varchar(100) not null default "",
  password      varchar(255) not null default "",
  f_data        smallint(1) not null default 0,
  primary key(id_user),
  key email(email),
)engine=MyISAM;

-- create table hak_akses(
--   id_hak_akses
--   nm_hak_akses
--   f_data
--   primary key(id_hak_akses)
-- )engine=MyISAM;

create table portofolio(
  id_portofolio        int(1) unsigned auto_increment,
  id_jenis_portofolio  int(1) not null default 0,
  nm_portofolio        varchar(100) not null default "",
  diskripsi            longtext,
  primary key(id_portofolio)
)engine=MyISAM;

create table jenis_portofolio(
  id_jenis_portofolio int(1) unsigned auto_increment,
  nm_jenis_portofolio varchar(15) not null default "",
  primary key(id_jenis_portofolio)
)engine=MyISAM;

create table konten(
  id_konten       int(1) unsigned auto_increment,
  id_jenis_konten int(1) not null default 0,
  nm_konten       varchar(65) not null default "",
  deskripsi       text,
  primary key(id_konten),
  key id_jenis_konten(id_jenis_konten)
)engine=MyISAM;

create table jenis_konten(
  id_jenis_konten int(1) unsigned auto_increment,
  nm_jenis_konten varchar(35) not null default "",
  ket             text,
  primary key(id_jenis_konten)
)engine=MyISAM;

create table berkas(
  id_berkas     int(1) unsigned auto_increment,
  id_item       int(1) not null default 0,
  id_uploader   int(1) not null default 0,
  nm_tabel      varchar(35) not null default "",
  nm_file_asli  varchar(150) not null default "",
  nm_file_unik  varchar(50) not null default "",
  ext           varchar(10) not null default "",
  size          int(1) not null default 0,
  uploaded      datetime,
  modified      datetime,
  keterangan    text,
  f_data        smallint(1) not null default 0,
  primary key(id_berkas),
  key id_item(id_item),
  key id_uploader(id_uploader)
)engine=MyISAM;

create table tim(
  id_tim  int(1) unsigned auto_increment,
  nm_tim  varchar(35) not null default "",
  jabatan varchar(150) not null default "",
  ket     text,
  primary key(id_tim)
)engine=MyISAM;

create table customer(
  id_customer int(1) unsigned auto_increment,
  nm_customer varchar(65) not null default "",
  primary key(id_customer)
)engine=MyIsam;

-- create table judul_konten(
--   id_judul_konten
--   nm_judul_konten
-- )engine=MyISAM;

-- create table konten(
--   id_konten
--   id_judul_konten
--   nm_konten

-- )engine=MyISAM;

INSERT INTO `webprofile-trisula`.`global_config` 
(`variable`, `profile`, `value`) 
VALUES 
('office', 'telp1', '0274-2838477'),
('office', 'telp2', '0822-2065-4963'),
('office', 'email1', 'office@trisulaindonesia.com'),
('office', 'email2', 'cv.trisulaindonesia@gmail.com'),
('office', 'alamat', 'Jln. Damai, Gg. Sunan Ampel III, No.3, Sinduharjo, Ngaglik, Sleman, D. I. Yogyakarta - 55581.'),

create table record_downloader(
  id_record   int(1) auto_increment unsigned,
  name        varchar(45) not null default "",
  email       varchar(45) not null default "",
  keterangan  text,
  date_record datetime,
  primary key(id_record)
)engine=MyISAM;